﻿using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;
using System.IO;

namespace SharpNav.PathCreator
{
    public class PathCreatorInitView : PathCreatorView
    {
        private TextView _pathLabel;

        public PathCreatorInitView(string path, PathCreatorWindow window) : base(path, window)
        {
            try
            {
                if (Root.FindChild("Path", out _pathLabel)) { }
                if (Root.FindChild("CreatePath", out Button createPath)) { createPath.Clicked = CreateClicked; }
                if (Root.FindChild("LoadPath", out Button loadPath)) { loadPath.Clicked = LoadClicked; }
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }
        }

        private void LoadClicked(object sender, ButtonBase e)
        {
            if (!File.Exists(_pathLabel.Text))
            {
                Chat.WriteLine($"File not found '{_pathLabel.Text}'");
                return;
            }

            Parent.SPath = SPath.Load(_pathLabel.Text);
            Load();
        }

        private void CreateClicked(object sender, ButtonBase e)
        {
            Parent.SPath = SPath.Create();
            Load();
        }

        private void Load()
        {
            if (Parent.SPath == null)
                return;

            Dispose();
            Parent.LoadMainView();
        }
    }
}
﻿using AOSharp.Common.GameData.UI;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System;

namespace SharpNav.PathCreator
{
    public class PathCreatorWindow : AOSharpWindow
    {
        private PathCreatorInitView _initView;
        private PathCreatorMainView _mainView;
        private View _root;
        public SPath SPath;

        public PathCreatorWindow(string windowName, string path, WindowStyle windowStyle = WindowStyle.Default, WindowFlags flags = WindowFlags.AutoScale | WindowFlags.NoFade) : base(windowName, path, windowStyle, flags)
        {
        }

        protected override void OnWindowCreating()
        {
            try
            {
                if (Window.FindView("Root", out _root))
                    LoadInitView();
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }
        }

        public void LoadInitView()
        {
            _initView = new PathCreatorInitView($"{Main.PluginDir}\\UI\\Views\\PathCreatorInitView.xml", this);
        }

        public void LoadMainView()
        {
            _mainView = new PathCreatorMainView($"{Main.PluginDir}\\UI\\Views\\PathCreatorMainView.xml", this);
        }

        public void AddChild(View view)
        {
            _root.AddChild(view, true);
        }

        public void RemoveChild(View view)
        {
            _root.RemoveChild(view);
        }
    }
}
﻿using AOSharp.Core;
using AOSharp.Core.UI;
using AOSharp.Pathfinding;
using System.Linq;

namespace SharpNav.PathCreator
{
    public class Main : AOPluginEntry
    {
        public static string PluginDir;
        private PathCreatorWindow _pathMakerWindow;

        public override void Run(string pluginDir)
        {
            Chat.WriteLine("SharpNav Path Creator");

            PluginDir = pluginDir;


            SMovementController.Set();

            Chat.RegisterCommand("pathadd", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SPathManager.Paths.FirstOrDefault()?.AddPoint();
                //SPathManager.Paths.FirstOrDefault(x=>x.Name ="TestPath")?.AddPoint();

            });

            Chat.RegisterCommand("pathdelete", (string command, string[] param, ChatWindow chatWindow) =>
            {
                SPathManager.Paths.FirstOrDefault()?.Delete();
            });


            _pathMakerWindow = new PathCreatorWindow("Creator", $"{pluginDir}\\UI\\Windows\\PathCreatorWindow.xml");
            _pathMakerWindow.Show();
        }
    }
}
﻿using AOSharp.Pathfinding;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace SharpNav.ConsoleApp
{
    internal class Loader
    {
        private readonly string[] _states = { "[o    ]", "[ o   ]", "[  o  ]", "[   o ]", "[    o]", "[   o ]", "[  o  ]", "[ o   ]" };
        private int _counter;
        private bool _stopped = false;
        private TimeSpan _elapsedTime;
        private Timer _timer;
        private readonly string _displayText;

        internal Loader(string filePath)
        {
            _displayText = $"Baking '{filePath}'";
        }

        private void Loop()
        {
            Console.CursorVisible = false;

            _timer = new Timer(UpdateElapsedTime, null, 0, 500);

            while (true)
            {
                Console.SetCursorPosition(0, 0);
                Console.Write($"{_displayText}\nElapsed Time: {_elapsedTime.ToString(@"hh\:mm\:ss")}\n{_states[_counter]} ");
                _counter = (_counter + 1) % _states.Length;
                Thread.Sleep(500);
                if (_stopped) break;
            }
        }

        private void UpdateElapsedTime(object state) => _elapsedTime = _elapsedTime.Add(TimeSpan.FromSeconds(0.5));

        public void Start() => Task.Run(() => Loop());

        public void Stop(out TimeSpan elapsedTime)
        {
            elapsedTime = _elapsedTime;
            _stopped = true;
            _timer.Dispose();
        }
    }
}
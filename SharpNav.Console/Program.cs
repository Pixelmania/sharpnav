﻿using AOSharp.Common.GameData;
using AOSharp.Pathfinding;
using SharpNav.Pathfinding;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SharpNav.ConsoleApp
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
               //  StandardBake();
                 BakeWithLinks();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.ReadLine();
        }

        private static async void StandardBake()
        {
            NavMeshGenConfig genSettings = NavMeshGenConfig.Load();
            var objPath = genSettings.ObjPath;
            var navPath = objPath.ChangePathExtension(".nav");

            var loader = new Loader(genSettings.ObjPath);
            loader.Start();

            await Task.Run(() =>
            {
                if (SNavMeshGenerator.GenerateFromObj(genSettings, objPath, out NavMesh navMesh))
                    SNavMeshSerializer.SaveToFile(navMesh, navPath);
            });

            loader.Stop(out TimeSpan elapsedTime);

            Console.Clear();
            Console.WriteLine($"Total time: {elapsedTime.ToString(@"hh\:mm\:ss")}\nNavmesh file saved at '{navPath}'");
        }

        private static async void BakeWithLinks()
        {
            NavMeshGenConfig genSettings = NavMeshGenConfig.Load();
            var objPath = genSettings.ObjPath;
            var navPath = objPath.ChangePathExtension(".nav");

            List<SoftLinkConnection> connections = new List<SoftLinkConnection>();


            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(268.8, 3.8, 169.3)), new StandOnDynelLink(new Vector3(272.0, 36.1, 161.0)), OffMeshConnectionFlags.None));
            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(275.1, 36.5, 171.3)), new StandOnDynelLink(new Vector3(271.4, 44.6, 199.6)), OffMeshConnectionFlags.None));
            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(283.1, 44.3, 179.7)), new StandOnDynelLink(new Vector3(270.3, 36.1, 160.8)), OffMeshConnectionFlags.None));
            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(268.7, 36.5, 171.6)), new StandOnDynelLink(new Vector3(270.1, 3.8, 165.2)), OffMeshConnectionFlags.None));


            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(171.1, 3.8, 279.3)), new StandOnDynelLink(new Vector3(167.8, 36.1, 266.8)), OffMeshConnectionFlags.None));
            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(174.4, 36.5, 272.6)), new StandOnDynelLink(new Vector3(207.0, 44.1, 266.9)), OffMeshConnectionFlags.None));
            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(183.9, 44.1, 278.5)), new StandOnDynelLink(new Vector3(167.0, 36.1, 264.6)), OffMeshConnectionFlags.None));
            connections.Add(new SoftLinkConnection(new StandOnDynelLink(new Vector3(229.9, 36.5, 266.3)), new StandOnDynelLink(new Vector3(164.3, 3.8, 275.4)), OffMeshConnectionFlags.None));


            connections.Add(new SoftLinkConnection(new JumpLink(new Vector3(235.4, 37.4, 189.2)), new JumpLink(new Vector3(256.5, 3.8, 189.0)), OffMeshConnectionFlags.None));
            connections.Add(new SoftLinkConnection(new JumpLink(new Vector3(202.0, 44.0, 178.9)), new JumpLink(new Vector3(176.8, 37.4, 176.5)), OffMeshConnectionFlags.None));


            var loader = new Loader(genSettings.ObjPath);
            loader.Start();


            await Task.Run(() =>
            {
                if (!SNavMeshGenerator.GenerateTiledNavMesh(ObjParser.ReadObjFile($"C:\\navmeshes\\152.obj"), genSettings, connections, out NavMesh navMsesh))
                    return;
                SNavMeshSerializer.SaveToFile(navMsesh, "C:\\navmeshes\\152.nav");
            });

            loader.Stop(out TimeSpan elapsedTime);

            Console.Clear();
            Console.WriteLine($"Total time: {elapsedTime.ToString(@"hh\:mm\:ss")}\nNavmesh file saved at '{navPath}'");
        }
    }
}
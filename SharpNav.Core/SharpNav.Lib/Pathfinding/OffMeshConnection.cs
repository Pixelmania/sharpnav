// Copyright (c) 2014-2015 Robert Rouhani <robert.rouhani@gmail.com> and other contributors (see CONTRIBUTORS file).
// Licensed under the MIT License - https://raw.github.com/Robmaister/SharpNav/master/LICENSE

using System;
using System.Collections.Generic;
using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Pathfinding;
using SharpNav.Geometry;
using AVector3 = AOSharp.Common.GameData.Vector3;
using SVector3 = SharpNav.Geometry.Vector3;

namespace SharpNav.Pathfinding
{
	/// <summary>
	/// A set of flags that define properties about an off-mesh connection.
	/// </summary>
	[Flags]
	public enum OffMeshConnectionFlags : byte
	{
		/// <summary>
		/// No flags.
		/// </summary>
		None = 0x0,

		/// <summary>
		/// The connection is bi-directional.
		/// </summary>
		Bidirectional = 0x1
	}

	/// <summary>
	/// An offmesh connection links two polygons, which are not directly adjacent, but are accessibly through
	/// other means (jumping, climbing, etc...).
	/// </summary>
	public class OffMeshConnection
	{
		/// <summary>
		/// Gets or sets the first endpoint of the connection
		/// </summary>
		internal SVector3 Pos0 { get; set; }

        /// <summary>
        /// Gets or sets the second endpoint of the connection
        /// </summary>
        internal SVector3 Pos1 { get; set; }

        /// <summary>
        /// Gets or sets the radius
        /// </summary>
        internal float Radius { get; set; }

        /// <summary>
        /// Gets or sets the polygon's index
        /// </summary>
        internal int Poly { get; set; }

        /// <summary>
        /// Gets or sets the polygon flag
        /// </summary>
        internal OffMeshConnectionFlags Flags { get; set; }

        /// <summary>
        /// Gets or sets the endpoint's side
        /// </summary>
        internal BoundarySide Side { get; set; }

        /// <summary>
        /// Gets or sets user data for this connection.
        /// </summary>
        internal SoftLinkType Tag { get; set; }

        public SoftLink SoftLink0 { get; set; }

        public SoftLink SoftLink1 { get; set; }

        public OffMeshConnection(SoftLink link0, SoftLink link1)
        {
            SoftLink0 = link0;
            SoftLink1 = link1;
            Pos0 = link0.Position;
            Pos1 = link1.Position;
            Flags = OffMeshConnectionFlags.None;
            Radius = 0.25f;
        }

        public OffMeshConnection()
        {

        }
    }
}
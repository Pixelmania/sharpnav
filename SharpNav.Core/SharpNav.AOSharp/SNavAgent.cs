﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Collections.Generic;
using AOSharp.Core.UI;
using SharpNav;
using SharpNav.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using static SharpNav.NavMeshQuery;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public class SNavAgent
    {
        public bool HasPathfinder => _pathFinder != null;
        public bool IsNavigating => Path.Count != 0;
        public NavMesh NavMesh => _pathFinder.NavMesh;

        internal Queue<Path> Path = new Queue<Path>();
        internal Path CurrentPath => Path.Peek();
        internal bool IsMoving => DynelManager.LocalPlayer.IsMoving;

        private SPathfinder _pathFinder;

        internal void LerpTowards(Vector3 pos, float minSpeed, float maxSpeed, float deltaTime)
        {
            Vector3 myPos = DynelManager.LocalPlayer.Position;
            myPos.Y = 0;
            Vector3 dstPos = pos;
            dstPos.Y = 0;

            if (Vector3.Distance(dstPos, myPos) < 0.01f)
                return;

            Quaternion fromRot = DynelManager.LocalPlayer.Rotation;
            Quaternion toRot = Quaternion.FromTo(myPos, dstPos);
            float dT = deltaTime * MathUtils.Remap(DynelManager.LocalPlayer.Velocity, 0, 13, minSpeed, maxSpeed);

            DynelManager.LocalPlayer.Rotation = MathUtils.Slerp(fromRot, toRot, dT);
        }

        internal AgentNavMeshState IsOnNavPoly(float radius, out Poly poly)
        {
            return _pathFinder.IsOnNavMesh(radius, out poly);
        }

        public bool FindNearestNavPoint(Vector3 position, Vector3 extents, out NavPoint point)
        {
            point = new NavPoint();

            if (PathFinderIsNull())
                return false;

            return _pathFinder.FindNearestPoint(position, extents, out point);
        }

        internal List<StraightPathVertex> GenerateNavPath(Vector3 startPos, Vector3 endPos)
        {
            if (PathFinderIsNull())
                return new List<StraightPathVertex>();

            return _pathFinder.GeneratePath(startPos, endPos);
        }

        //Right now doesn't work with soft links
        internal List<StraightPathVertex> GenerateSmoothNavPath(Vector3 startPos, Vector3 endPos, float density)
        {
            if (PathFinderIsNull())
                return new List<StraightPathVertex>();

            return _pathFinder.GenerateSmoothPath(startPos, endPos, density);
        }

        internal void SetSmoothNavDestination(Vector3 endPos, float density)
        {
            if (PathFinderIsNull())
                return;

            SMovementController.SetWaypoints(GenerateSmoothNavPath(DynelManager.LocalPlayer.Position, endPos, density));
        }

        internal void SetNavDestination(Vector3 endPos)
        {
            if (PathFinderIsNull())
                return;

            SMovementController.SetWaypoints(GenerateNavPath(DynelManager.LocalPlayer.Position, endPos));
        }

        internal void SetNavDestination(Vector3 endPos, IEnumerable<Dynel> dynelsToAvoid, float avoidRadius, bool navPolyCheckOnObstacles, bool debugMode)
        {
            if (PathFinderIsNull())
                return;

            List<PolyCircle> polyCircles = dynelsToAvoid
                .Where(x => x.Identity != DynelManager.LocalPlayer.Identity)
                .Select(x => new PolyCircle(x.Position, avoidRadius))
                .ToList();

            List<ConvexHull> convexHulls = GeometryUtils.GroupByIntersection(polyCircles)
                .Select(x => new ConvexHull(x.SelectMany(y => y.Verts)))
                .ToList();

            List<Vector3> originalRoute = GenerateNavPath(DynelManager.LocalPlayer.Position, endPos).Select(x => x.Position).ToList();
            originalRoute.Insert(0, DynelManager.LocalPlayer.Position);

            List<SegmentedLine> route = new List<SegmentedLine>();
            float padding = 0.325f;

            for (int x = 0; x < originalRoute.Count() - 1; x++)
            {
                route.Add(new SegmentedLine(originalRoute[x], originalRoute[x + 1]));
            }

            // Split our segmented line among hull intersection, also ensure no line starts / ends within a hull

            foreach (SegmentedLine line in route)
            {
                foreach (ConvexHull hull in convexHulls)
                {
                    if (debugMode)
                        hull.Draw(DebuggingColor.White);

                    if (!hull.Cut(line.Start, line.End, out var intersections))
                        continue;

                    foreach (var intersection in intersections)
                        line.Split(intersection, hull);

                    if (hull.IsPointContained(line.Start))
                        line.RemoveFirstVert();

                    if (hull.IsPointContained(line.End))
                        line.RemoveLastVert();
                }
            }

            // Skip empty lines, also skip lines that are fully contained within a hull

            SegmentedLine segmentedLine = new SegmentedLine();

            for (int x = 0; x < route.Count - 1; x++)
            {
                var currLine = route[x].Verts;
                var nextLine = route[x + 1].Verts;

                segmentedLine.Append(route[x]);

                if (currLine.Count == 0 || nextLine.Count == 0)
                    continue;

                if (currLine[0].Hull != nextLine[0].Hull)
                    continue;

                route[x].Verts.Add(nextLine[0]);
            }

            // Combine into one line, and perform a distinct operation

            segmentedLine.Append(route[route.Count - 1]);
            segmentedLine.MergeByDistance();

            var playerPos = DynelManager.LocalPlayer.Position;

            List<Vector3> finalRoute = new List<Vector3>();

            // Is player in a hull

            foreach (var hull in convexHulls)
            {
                if (!hull.IsPointContained(playerPos))
                    continue;

                foreach (var ver in segmentedLine.Verts.ToList())
                {
                    if (ver.Hull == null)
                    {
                        segmentedLine.Verts.Remove(ver);
                        continue;
                    }

                    break;
                }

                if (segmentedLine.Verts.Count == 0)
                {
                    var closestPos = hull._verts.OrderBy(x => x.DistanceFrom(playerPos)).FirstOrDefault();

                    if (Vector3.Distance(new Vector3(playerPos.X, 0, playerPos.Z), new Vector3(closestPos.X, 0, closestPos.Z)) < padding)
                        return;

                    SMovementController.SetDestination(closestPos);

                    return;
                }

                List<Vector3> obstaclePath = new List<Vector3>();

                if (!navPolyCheckOnObstacles)
                {
                    obstaclePath = hull.FindQuickestPath(playerPos, segmentedLine.Verts[0].Cut);
                }
                else
                {
                    obstaclePath = FindQuickestNavHullPath(hull, playerPos, segmentedLine.Verts[0].Cut);
                }

                if (obstaclePath.Count != 1)
                {
                    finalRoute.AddRange(obstaclePath);

                    if (debugMode)
                        DebugUtils.DrawOutline(obstaclePath, DebuggingColor.Yellow);
                }
                else
                {
                    var p1 = playerPos;
                    var p2 = segmentedLine.Verts[0].Cut;
                    finalRoute.AddRange(new List<Vector3> { p1, p2 });

                    if (debugMode)
                        Debug.DrawLine(p1 + Vector3.Up, p2 + Vector3.Up, DebuggingColor.Yellow);
                }

                break;
            }

            // For any line segment that is inside a hull, curve around it

            for (int i = 0; i < segmentedLine.Verts.Count - 1; i++)
            {
                var currPoint = segmentedLine.Verts[i];
                var nextPoint = segmentedLine.Verts[i + 1];

                if (currPoint.Hull == nextPoint.Hull)
                {
                    if (currPoint.Hull != null)
                    {
                        var obstaclePath = new List<Vector3>();

                        if (!navPolyCheckOnObstacles)
                        {
                            obstaclePath = currPoint.Hull.FindQuickestPath(currPoint.Cut, nextPoint.Cut);
                            finalRoute.AddRange(obstaclePath);
                        }
                        else
                        {
                            obstaclePath = FindQuickestNavHullPath(currPoint.Hull, currPoint.Cut, nextPoint.Cut);
                            finalRoute.AddRange(obstaclePath);
                        }

                        if (debugMode)
                            DebugUtils.DrawOutline(obstaclePath, DebuggingColor.Yellow);
                    }
                    else
                    {
                        finalRoute.AddRange(new List<Vector3> { currPoint.Cut, nextPoint.Cut });

                        if (debugMode)
                            DebugUtils.DrawLineWithDots(currPoint.Cut, nextPoint.Cut, DebuggingColor.Yellow);
                    }
                }
                else
                {
                    finalRoute.AddRange(new List<Vector3> { currPoint.Cut, nextPoint.Cut });

                    if (debugMode)
                        DebugUtils.DrawLineWithDots(currPoint.Cut, nextPoint.Cut, DebuggingColor.Yellow);
                }
            }

            var finalPoint = finalRoute.LastOrDefault();
            finalPoint.Y = 0;

            if (finalRoute.Count == 2 && Vector3.Distance(new Vector3(playerPos.X, 0, playerPos.Z), finalPoint) < padding)
                return;

            SMovementController.SetWaypoints(finalRoute);
        }

        private List<Vector3> FindQuickestNavHullPath(ConvexHull hull, Vector3 startPos, Vector3 endPos)
        {
            var obstaclePath = hull.FindQuickestPath(startPos, endPos);
            var navObstaclePath = new List<Vector3>();

            foreach (var pos in obstaclePath)
            {
                if (!FindNearestNavPoint(pos, SMovementController.Extents, out NavPoint point))
                {
                    navObstaclePath.Add(pos);
                    Chat.WriteLine("Couldn't find a valid polygon near a obstacle");
                    continue;
                }

                navObstaclePath.Add(point.Position.ToVector3());
            }

            return navObstaclePath;
        }

        internal void SetRandomDestination()
        {
            if (PathFinderIsNull())
                return;

            SMovementController.SetWaypoints(_pathFinder.GeneratePath(DynelManager.LocalPlayer.Position, GetRandomPoint()));
        }

        internal Vector3 GetRandomPoint()
        {
            return _pathFinder.GetRandomPoint().Position.ToVector3();
        }

        internal void AppendNavDestination(Vector3 startPos, Vector3 endPos)
        {
            if (PathFinderIsNull())
                return;

            SMovementController.AppendWaypoints(GenerateNavPath(startPos, endPos));
        }

        internal void AppendPath(SPath sPath)
        {
            SMovementController.AppendWaypoints(sPath.GetWaypoints(out _));
        }

        internal void SetPath(SPath sPath)
        {
            SMovementController.SetWaypoints(sPath.GetWaypoints(out _));
        }

        internal void Stop()
        {
            Path.Clear();
            SMovementController.SetMovement(MovementAction.FullStop);
        }

        internal void FollowTarget(Identity identity)
        {
            Network.Send(new FollowTargetMessage
            {
                Type = FollowTargetType.Target,
                Info = new FollowTargetMessage.TargetInfo
                {
                    Target = identity
                }
            });
        }

        internal void ResetPathfinder()
        {
            _pathFinder = null;
        }

        /// <summary>
        /// Generates a path from given coordinate and returns the path distance.
        /// </summary>
        internal bool GetDistance(Vector3 destination, out float distance)
        {
            distance = 0;

            if (PathFinderIsNull())
                return false;

            try
            {
                List<StraightPathVertex> path = _pathFinder.GeneratePath(DynelManager.LocalPlayer.Position, destination);

                for (int i = 0; i < path.Count - 1; i++)
                    distance += Vector3.Distance(path[i].Position, path[i + 1].Position);

                return true;
            }
            catch
            {
                return false;
            }
        }

        internal bool TryGetNavmeshPfId(out int pfId)
        {
            pfId = 0;

            if (_pathFinder == null)
                return false;

            pfId = _pathFinder.PlayfieldId;

            return true;
        }

        /// <summary>
        /// Sets NavMesh
        /// </summary>
        internal void SetNavmesh(NavMesh navmesh, bool forceSet = false)
        {
            if (navmesh == null)
                return;

            if (!forceSet)
            {
                if (_pathFinder?.PlayfieldId == Playfield.ModelIdentity.Instance)
                    return;
            }

            _pathFinder = new SPathfinder(navmesh, Playfield.ModelIdentity.Instance);
        }

        /// <summary>
        /// Clears current NavMesh
        /// </summary>
        internal void DeleteNavmesh()
        {
            Stop();
            _pathFinder = null;
        }

        private bool PathFinderIsNull()
        {
            if (_pathFinder == null)
            {
                Chat.WriteLine("Pathfinder is null (navmesh was not set?)");
                return true;
            }

            return false;
        }

        internal AgentNavMeshState FindPolys(int distance, out Poly[] polies)
        {
            return _pathFinder.FindPolys(distance, out polies);
        }
    }
}

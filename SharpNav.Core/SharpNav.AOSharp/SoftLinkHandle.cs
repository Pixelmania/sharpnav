﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using SharpNav;
using SharpNav.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using AOSharp.Core.UI;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public class SoftLinkHandle
    {
        private StraightPathVertex _prevWaypoint;
        private StraightPathVertex _lastWaypoint;

        public SoftLinkHandle()
        {
            _prevWaypoint = new StraightPathVertex();

            Network.N3MessageReceived += OnN3MessageReceived;
        }

        private void OnN3MessageReceived(object sender, N3Message n3Msg)
        {
            if (!(n3Msg is N3TeleportMessage n3TeleportMsg))
                return;

            if (n3TeleportMsg.Identity != DynelManager.LocalPlayer.Identity)
                return;

            if (_lastWaypoint.Position == Vector3.Zero)
                return;

            SMovementController.SetNavDestination(_lastWaypoint.Position);
            _lastWaypoint = new StraightPathVertex();
        }

        public void Update(StraightPathVertex waypoint)
        {
            if (waypoint.Flags != StraightPathFlags.OffMeshConnection)
                return;

            var navMesh = SMovementController.NavAgent.NavMesh;

            if (navMesh == null)
                return;

            if (!SMovementController.NavAgent.NavMesh.GetOffMeshConnection(_prevWaypoint.Point.Polygon, waypoint.Point.Polygon, out OffMeshConnection connection))
                return;

            switch (connection.SoftLink0.SoftLinkType)
            {
                case SoftLinkType.Jump:
                    SMovementController.SetMovement(MovementAction.JumpStart);
                    break;
                case SoftLinkType.StandOn:
                    if (SMovementController.NavAgent.CurrentPath.PeekLastWaypoint(out _lastWaypoint))
                        SMovementController.Halt();
                    break;
                case SoftLinkType.Use:
                    var closestTerminal = DynelManager.AllDynels
                        .Where(x => x.Identity.Type == IdentityType.Terminal && x.Name == ((UsableDynelLink)connection.SoftLink0).Name)
                        .OrderBy(x => x.DistanceFrom(DynelManager.LocalPlayer))
                        .FirstOrDefault();
                    closestTerminal?.Use();
                    break;
                default:
                    break;
            }

            _prevWaypoint = waypoint;
        } 
    }
}
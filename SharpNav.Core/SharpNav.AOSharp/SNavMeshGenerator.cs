﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System;
using AOSharp.Core.UI;
using SharpNav;
using System.Diagnostics;
using STriangle3 = SharpNav.Geometry.Triangle3;
using System.Threading.Tasks;
using SharpNav.Geometry;
using SharpNav.Pathfinding;
using AVector3 = AOSharp.Common.GameData.Vector3;
using SVector3 = SharpNav.Geometry.Vector3;

namespace AOSharp.Pathfinding
{
    public class SNavMeshGenerator
    {
        public static Action<BakeState> BakeStatus;
        public async static Task<NavMesh> GenerateFromTrisAsync(NavMeshGenerationSettings settings, List<STriangle3> tris)
        {
            try
            {
                return await Task.Run(() => GenerateFromTris(settings, new List<SoftLinkConnection>(), tris));
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        public static NavMesh GenerateFromTris(NavMeshGenerationSettings settings, List<SoftLinkConnection> connections, List<STriangle3> tris)
        {
            NavMesh navMesh = null;

            try
            {
                navMesh = Generate(tris, connections, settings);
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }

            return navMesh;
        }

        public static NavMesh GenerateFromTris(NavMeshGenerationSettings settings, List<STriangle3> tris)
        {
            return GenerateFromTris(settings, new List<SoftLinkConnection>(), tris);
        }

        public static bool GenerateFromObj(NavMeshGenerationSettings settings, string filePath, out NavMesh navMesh)
        {
            return (navMesh = GenerateFromTris(settings, new List<SoftLinkConnection>(), ObjParser.ReadObjFile(filePath))) != null;
        }

        public static bool GenerateFromObj(NavMeshGenerationSettings settings, List<SoftLinkConnection> connections, string filePath, out NavMesh navMesh)
        {
            return (navMesh = GenerateFromTris(settings, connections, ObjParser.ReadObjFile(filePath))) != null;
        }

        /// <summary>
        /// An alternative way to generate dungeon navmeshes with some filtering options.
        /// </summary>

        public async static Task<List<DungeonNavMesh>> GenerateDungeonAsync(NavMeshGenerationSettings settings)
        {
            if (!Playfield.IsDungeon)
                return null;

            List<DungeonNavMesh> navMeshes = new List<DungeonNavMesh>();

            try
            {
                var triGeom = await Task.Run(() => TerrainData.GetPerFloorDungeonGeometry(settings.Bounds));

                foreach (var geo in triGeom)
                    navMeshes.Add(new DungeonNavMesh { Floor = geo.Key, NavMesh = await GenerateAsync(settings, geo.Value) });

                return navMeshes;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        /// <summary>
        /// The main method to generate navmeshes of any map type.
        /// </summary>
        public async static Task<NavMesh> GenerateAsync(NavMeshGenerationSettings settings)
        {
            try
            {
                var triGeom = await Task.Run(() => TerrainData.GetTriGeometry(settings.Bounds));
                return await GenerateAsync(settings, triGeom);
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        private async static Task<NavMesh> GenerateAsync(NavMeshGenerationSettings settings, List<STriangle3> triMesh)
        {
            Stopwatch sw = Stopwatch.StartNew();

            try
            {
                BakeStatus?.Invoke(BakeState.Generating);
                var navMeshBake = await Task.Run(() => Generate(triMesh, new List<SoftLinkConnection>(), settings));

                Chat.WriteLine($"Total time: {sw.ElapsedMilliseconds.FormatTime()}", ChatColor.Green);
                sw.Stop();

                BakeStatus?.Invoke(BakeState.Finished);

                return navMeshBake;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return null;
            }
        }

        public static bool Generate(NavMeshGenerationSettings settings, List<SoftLinkConnection> connections, out NavMesh navMesh)
        {
            Stopwatch sw = Stopwatch.StartNew();
            navMesh = null;

            try
            {
                List<STriangle3> triMesh = TerrainData.GetTriGeometry(settings.Bounds);

                navMesh = Generate(triMesh, connections, settings);

                Chat.WriteLine($"Navmesh generation finished.", ChatColor.Green);
                Chat.WriteLine($"Total time: {sw.ElapsedMilliseconds.FormatTime()}", ChatColor.Green);
                sw.Stop();

                return true;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return false;
            }
        }

        public static bool GenerateBuilder(NavMeshGenerationSettings settings, Room room, out NavMeshBuilder builder)
        {
            builder = null;

            try
            {
                List<STriangle3> triMesh = TerrainData.GetTriGeometry(room);
                List<OffMeshConnection> offMeshConnections = new List<OffMeshConnection>();

                for (int i = 0; i < room.NumDoors; i++)
                {
                    //if (room.Instance != 6)
                    //    break;

                    //if (i != 1)
                    //    continue;

                    room.GetDoorPosRot(i, out var pos, out var rot);

                    offMeshConnections.Add(new OffMeshConnection
                    {
                        Pos0 = (pos + rot.Forward).ToSharpNav(),
                        Pos1 = (pos - rot.Forward).ToSharpNav(),
                        Flags = OffMeshConnectionFlags.None,
                        Radius = 0.25f
                    });
                }

                triMesh = triMesh.Select(x => new STriangle3
                {
                    A = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.A.ToVector3() - room.Position)).ToSharpNav(),
                    B = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.B.ToVector3() - room.Position)).ToSharpNav(),
                    C = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (x.C.ToVector3() - room.Position)).ToSharpNav(),
                }).ToList();

                foreach (OffMeshConnection con in offMeshConnections)
                {
                    con.Pos0 = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (con.Pos0.ToVector3() - room.Position)).ToSharpNav();
                    con.Pos1 = (Quaternion.AngleAxis(360 - room.Rotation, AVector3.Up) * (con.Pos1.ToVector3() - room.Position)).ToSharpNav();
                }

                builder = GenerateBuilder(triMesh, offMeshConnections.ToArray(), settings);

                return true;
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message, ChatColor.Red);
                return false;
            }
        }

        private static void BuildCompactHeightfield(IEnumerable<STriangle3> triangles, NavMeshGenerationSettings settings, out CompactHeightfield chf)
        {
            Stopwatch sw = Stopwatch.StartNew();

            BBox3 bounds = triangles.GetBoundingBox(settings.CellSize);

            Chat.WriteLine($"Loaded bounds. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            Chat.WriteLine($"Bounds: {bounds}", ChatColor.LightBlue);
            var hf = new Heightfield(bounds, settings);

            Chat.WriteLine($"Pre-Rasterizing Triangles. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);
            hf.RasterizeTriangles(triangles);

            Chat.WriteLine($"Rasterizing Triangles. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            hf.FilterLedgeSpans(settings.VoxelAgentHeight, settings.VoxelMaxClimb);
            hf.FilterLowHangingWalkableObstacles(settings.VoxelMaxClimb);
            hf.FilterWalkableLowHeightSpans(settings.VoxelAgentHeight);

            Chat.WriteLine($"Loaded heightfield. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            chf = new CompactHeightfield(hf, settings);
            chf.Erode(settings.VoxelAgentRadius);
            chf.BuildDistanceField();
            chf.BuildRegions(2, settings.MinRegionSize, settings.MergedRegionSize, settings.FilterLargestSection);
            Chat.WriteLine($"Loaded compact heightfield. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);
        }


        public static void GeneratePolyMesh(IEnumerable<STriangle3> triangles, NavMeshGenerationSettings settings, out PolyMesh polyMesh, out PolyMeshDetail detailMesh)
        {
            BuildCompactHeightfield(triangles, settings, out CompactHeightfield chf);
            polyMesh = new PolyMesh(chf.BuildContourSet(settings), settings);
            detailMesh = new PolyMeshDetail(polyMesh, chf, settings);
        }

        public static void GeneratePolyMesh(IEnumerable<STriangle3> triangles, NavMeshGenerationSettings settings, out PolyMesh polyMesh)
        {
            BuildCompactHeightfield(triangles, settings, out CompactHeightfield chf);
            polyMesh = new PolyMesh(chf.BuildContourSet(settings), settings);
        }

        public static bool GenerateTiledNavMesh(NavMeshGenerationSettings settings, IEnumerable<SoftLinkConnection> softLinks, out NavMesh navMesh)
        {
            return GenerateTiledNavMesh(TerrainData.GetTriGeometry(settings.Bounds), settings, softLinks, out navMesh);
        }
        /// <summary>
        /// Generates a <see cref="NavMesh"/> given a collection of triangles, soft links and some settings.
        /// </summary>
        /// 
        public static bool GenerateTiledNavMesh(IEnumerable<STriangle3> triangles, NavMeshGenerationSettings settings, IEnumerable<SoftLinkConnection> softLinks, out NavMesh navMesh)
        {
            BuildCompactHeightfield(triangles, settings, out CompactHeightfield chf);

            int i = 0;

            var offMeshLinks = softLinks.SelectMany(x => x.Connections);

            List<NavMeshBuilder> builderCache = new List<NavMeshBuilder>();
            ContourSet contourSet = chf.BuildContourSet(settings);
            var contoursByConnectivity = GeometryUtils.GroupContoursByConnectivity(contourSet);
          
            foreach (var connectedContourSet in contoursByConnectivity)
            {
                bool skipMesh = true;
                List<SoftLinkConnection> filteredConnections = new List<SoftLinkConnection>();
                BBox3 newBounds = GeometryUtils.CalculateBounds(connectedContourSet, contourSet.Bounds, settings.CellSize, settings.CellHeight);
                Heightfield heightField = new Heightfield(newBounds, settings.CellSize, settings.CellHeight);
                ContourSet newContour = new ContourSet(connectedContourSet, contourSet.Bounds, contourSet.Width, contourSet.Height);
                PolyMesh regionPolyMesh = new PolyMesh(newContour, settings);
                NavMeshQuery tempQuery = new NavMeshQuery(new NavMesh(new NavMeshBuilder(regionPolyMesh, null, new OffMeshConnection[0], settings)), 8192);

                SVector3 extents = new SVector3(2, 2, 2);

                foreach (var link in offMeshLinks)
                {
                    SVector3 startPoint0 = link.Pos0;

                    tempQuery.FindNearestPoly(ref startPoint0, ref extents, out var p1);

                    if (p1.Position != SVector3.Zero)
                    {
                        filteredConnections.Add(softLinks.FirstOrDefault(x => x.Connections.Contains(link)));
                        skipMesh = false;
                    }

                    if (skipMesh)
                    {
                        SVector3 startPoint1 = link.Pos1;

                        tempQuery.FindNearestPoly(ref startPoint1, ref extents, out var p2);

                        if (p2.Position != SVector3.Zero)
                            skipMesh = false;
                    }
                }

                if (skipMesh)
                    continue;

                NavMeshBuilder builder = new NavMeshBuilder(regionPolyMesh, null, filteredConnections.ToArray(), settings);

                builder.Header.Layer = i++;
                builderCache.Add(builder);
            }

            Chat.WriteLine($"Total tiles: {builderCache.Count}");

            navMesh = new NavMesh(SVector3.Zero, 3000, 3000, builderCache.Count, short.MaxValue / 4);

            foreach (var builder in builderCache)
                navMesh.AddTile(builder);

            foreach (NavTile tiles in navMesh.Tiles)
                navMesh.InitTile(tiles);


            navMesh.Settings = settings;    
            return navMesh != null;
        }


        public static NavMeshBuilder GenerateBuilder(IEnumerable<STriangle3> triangles, OffMeshConnection[] offMeshConnections, NavMeshGenerationSettings settings)
        {
            GeneratePolyMesh(triangles, settings, out PolyMesh polyMesh);
            return new NavMeshBuilder(polyMesh, null, offMeshConnections, settings);
        }

        /// <summary>
		/// Generates a <see cref="NavMesh"/> given a collection of triangles and some settings.
		/// </summary>
		/// <param name="triangles">The triangles that form the level.</param>
		/// <param name="settings">The settings to generate with.</param>
		/// <returns>A <see cref="NavMesh"/>.</returns>
		private static NavMesh Generate(IEnumerable<STriangle3> triangles, List<SoftLinkConnection> connections, NavMeshGenerationSettings settings)
        {
            NavMeshBuilder builder = GenerateBuilder(triangles, connections.SelectMany(x => x.Connections).ToArray(), settings);
            var navMesh = new NavMesh(builder);
            navMesh.Settings = settings;

            return navMesh;
        }
    }

    public enum BakeState
    {
        Generating,
        Finished
    }

    public class DungeonNavMesh
    {
        public int Floor;
        public NavMesh NavMesh;
    }
}
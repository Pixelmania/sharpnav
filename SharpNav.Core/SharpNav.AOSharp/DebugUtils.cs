﻿using AOSharp.Core;
using System.Collections.Generic;
using AOSharp.Common.GameData;

public class DebugUtils
{
    public static void DrawOutline(List<Vector3> verts, Vector3 color, float sphereSize = 0.125f)
    {
        if (verts.Count == 0)
            return;

        Vector3 offset = Vector3.Up;

        for (int i = 0; i < verts.Count - 1; i++)
        {
            Debug.DrawLine(verts[i] + offset, verts[i + 1] + offset, color);
            Debug.DrawSphere(verts[i] + offset, sphereSize, color);
        }

        Debug.DrawSphere(verts[verts.Count - 1] + offset, sphereSize, color);
    }

    public static void DrawLineWithDots(Vector3 v1, Vector3 v2, Vector3 color, float sphereSize = 0.125f)
    {
        Vector3 offset = Vector3.Up;

        Debug.DrawLine(v1 + offset, v2 + offset, color);
        Debug.DrawSphere(v1 + offset, sphereSize, color);
        Debug.DrawSphere(v2 + offset, sphereSize, color);
    }
}
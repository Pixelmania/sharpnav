﻿using System.Collections.Generic;

namespace AOSharp.Pathfinding
{
    public class UnionSearch
    {
        private readonly Dictionary<int, int> _parent = new Dictionary<int, int>();
        private readonly Dictionary<int, int> _rank = new Dictionary<int, int>();

        public void Add(int x)
        {
            if (!_parent.ContainsKey(x))
            {
                _parent[x] = x;
                _rank[x] = 0;
            }
        }

        public int Find(int x)
        {
            if (_parent[x] != x)
            {
                _parent[x] = Find(_parent[x]);
            }
            return _parent[x];
        }

        public void Union(int x, int y)
        {
            int rootX = Find(x);
            int rootY = Find(y);

            if (rootX != rootY)
            {
                if (_rank[rootX] > _rank[rootY])
                {
                    _parent[rootY] = rootX;
                }
                else if (_rank[rootX] < _rank[rootY])
                {
                    _parent[rootX] = rootY;
                }
                else
                {
                    _parent[rootY] = rootX;
                    _rank[rootX]++;
                }
            }
        }
    }
}
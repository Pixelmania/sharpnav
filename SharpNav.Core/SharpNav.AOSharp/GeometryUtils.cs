﻿using AOSharp.Common.GameData;
using SharpNav;
using SharpNav.Geometry;
using System;
using System.Collections.Generic;
using System.Linq;
using static SharpNav.PolyMesh;
using SVector3 = SharpNav.Geometry.Vector3;
using AVector3 = AOSharp.Common.GameData.Vector3;

namespace AOSharp.Pathfinding
{
    internal class GeometryUtils
    {
        public static List<List<Contour>> GroupContoursByConnectivity(ContourSet contourSet)
        {
            var contours = new List<Contour>(contourSet); // Assuming ContourSet has a property Contours
            var unionFind = new UnionSearch();
            var contourIndexMap = new Dictionary<Contour, int>();

            for (int i = 0; i < contours.Count; i++)
            {
                var contour = contours[i];
                contourIndexMap[contour] = i;
                unionFind.Add(i);
            }

            for (int i = 0; i < contours.Count; i++)
            {
                for (int j = i + 1; j < contours.Count; j++)
                {
                    var cont1 = contours[i];
                    var cont2 = contours[j];
                    if (AreContoursTouching(cont1, cont2))
                    {
                        unionFind.Union(i, j);
                    }
                }
            }

            var groups = new Dictionary<int, List<Contour>>();
            for (int i = 0; i < contours.Count; i++)
            {
                int root = unionFind.Find(i);
                if (!groups.ContainsKey(root))
                {
                    groups[root] = new List<Contour>();
                }
                groups[root].Add(contours[i]);
            }

            return groups.Values.ToList();
        }

        private static bool AreContoursTouching(Contour c1, Contour c2)
        {
            foreach (var vertex1 in c1.Vertices)
            {
                foreach (ContourVertex vertex2 in c2.Vertices)
                {
                    if (vertex1.X == vertex2.X && vertex1.Y == vertex2.Y && vertex1.Z == vertex2.Z)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static BBox3 CalculateBounds(IEnumerable<Contour> contours, BBox3 oldBounds, float cellSize, float cellHeight)
        {
            BBox3 bounds = new BBox3();
            bounds.Min = new  SVector3(int.MaxValue, int.MaxValue, int.MaxValue);
            foreach (var contour in contours)
            {
                foreach (var vertex in contour.Vertices)
                {
                    bounds.Expand(vertex.ToVector3(oldBounds, cellSize, cellHeight));
                }
            }
            return bounds;
        }

        private static PolyMesh CreatePolyMeshFromComponent(PolyMesh original, List<int> component)
        {
            var newVertices = new List<PolyVertex>();
            var newPolygons = new List<Polygon>();
            var vertexMapping = new Dictionary<int, int>();

            foreach (var polyIndex in component)
            {
                var originalPolygon = original.Polys[polyIndex];
                var newPolygonVertices = new List<int>();

                foreach (var vertexIndex in originalPolygon.Vertices)
                {
                    if (!vertexMapping.ContainsKey(vertexIndex))
                    {
                        vertexMapping[vertexIndex] = newVertices.Count;
                        newVertices.Add(original.Verts[vertexIndex]);
                    }
                    newPolygonVertices.Add(vertexMapping[vertexIndex]);
                }

                newPolygons.Add(new Polygon(newPolygonVertices.ToArray(), originalPolygon.NeighborEdges, originalPolygon.Area, originalPolygon.RegionId));
            }

            return new PolyMesh(
                newVertices.ToArray(),
                newPolygons.ToArray(),
                original.Bounds,
                original.NumVertsPerPoly,
                original.CellSize,
                original.CellHeight,
                original.BorderSize
            );
        }

        public static List<PolyMesh> SplitPolyMeshByConnectivity(PolyMesh polyMesh)
        {
            var connectedComponents = new List<List<int>>();
            var visitedPolygons = new HashSet<int>();

            for (int i = 0; i < polyMesh.Polys.Length; i++)
            {
                if (!visitedPolygons.Contains(i))
                {
                    var component = new List<int>();
                    var stack = new Stack<int>();
                    stack.Push(i);

                    while (stack.Count > 0)
                    {
                        var polygonIndex = stack.Pop();
                        if (!visitedPolygons.Contains(polygonIndex))
                        {
                            visitedPolygons.Add(polygonIndex);
                            component.Add(polygonIndex);

                            foreach (var neighborIndex in GetConnectedPolygons(polyMesh, polygonIndex))
                            {
                                if (!visitedPolygons.Contains(neighborIndex))
                                {
                                    stack.Push(neighborIndex);
                                }
                            }
                        }
                    }

                    connectedComponents.Add(component);
                }
            }

            var polyMeshes = new List<PolyMesh>();
            foreach (var component in connectedComponents)
            {
                var newPolyMesh = CreatePolyMeshFromComponent(polyMesh, component);
                polyMeshes.Add(newPolyMesh);
            }

            return polyMeshes;
        }

        private static List<int> GetConnectedPolygons(PolyMesh polyMesh, int polygonIndex)
        {
            var connectedPolygons = new List<int>();
            var currentPolygon = polyMesh.Polys[polygonIndex];

            for (int i = 0; i < polyMesh.Polys.Length; i++)
            {
                if (i == polygonIndex) continue;

                var otherPolygon = polyMesh.Polys[i];

                if (ArePolygonsConnected(polyMesh, currentPolygon, otherPolygon))
                {
                    connectedPolygons.Add(i);
                }
            }

            return connectedPolygons;
        }

        private static bool ArePolygonsConnected(PolyMesh polyMesh, Polygon polygonA, Polygon polygonB)
        {
            var edgesA = GetEdges(polygonA, polyMesh.Verts);
            var edgesB = GetEdges(polygonB, polyMesh.Verts);

            foreach (var edgeA in edgesA)
            {
                foreach (var edgeB in edgesB)
                {
                    if (edgeA.Equals(edgeB))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private static HashSet<PolyVertexEdge> GetEdges(Polygon polygon, PolyVertex[] vertices)
        {
            var edges = new HashSet<PolyVertexEdge>();
            for (int i = 0; i < polygon.Vertices.Length; i++)
            {
                var v1 = vertices[polygon.Vertices[i]];
                var v2 = vertices[polygon.Vertices[(i + 1) % polygon.Vertices.Length]];

                var edge = new PolyVertexEdge(v1, v2);
                edges.Add(edge);
            }
            return edges;
        }

        public static AVector3 GetIntersectionPoint(AVector3 a1, AVector3 a2, AVector3 b1, AVector3 b2)
        {
            float kA = (a2.Z - a1.Z) / (a2.X - a1.X);
            float kB = (b2.Z - b1.Z) / (b2.X - b1.X);

            float mA = a1.Z - kA * a1.X;
            float mB = b1.Z - kB * b1.X;

            float x = (mB - mA) / (kA - kB);
            float z = kA * x + mA;

            return new AVector3(x, a1.Y, z);
        }

        public static bool Intersects(AVector3 a1, AVector3 a2, AVector3 b1, AVector3 b2, out AVector3 point)
        {
            point = GetIntersectionPoint(a1, a2, b1, b2);

            bool withinA = (Math.Min(a1.X, a2.X) <= point.X && point.X <= Math.Max(a1.X, a2.X)) &&
                           (Math.Min(a1.Z, a2.Z) <= point.Z && point.Z <= Math.Max(a1.Z, a2.Z));

            bool withinB = (Math.Min(b1.X, b2.X) <= point.X && point.X <= Math.Max(b1.X, b2.X)) &&
                           (Math.Min(b1.Z, b2.Z) <= point.Z && point.Z <= Math.Max(b1.Z, b2.Z));

            return withinA && withinB;
        }

        public static List<List<PolyCircle>> GroupByIntersection(List<PolyCircle> polycircles)
        {
            Dictionary<PolyCircle, List<PolyCircle>> groups = new Dictionary<PolyCircle, List<PolyCircle>>();

            foreach (var circle in polycircles)
            {
                groups[circle] = new List<PolyCircle> { circle };
            }

            foreach (var circle1 in polycircles)
            {
                foreach (var circle2 in polycircles)
                {
                    if (circle1 == circle2)
                        continue;

                    if (!circle1.Intersect(circle2))
                        continue;

                    var group1 = groups[circle1];
                    var group2 = groups[circle2];

                    if (group1 == group2)
                        continue;

                    group1.AddRange(group2);

                    foreach (var circle in group2)
                        groups[circle] = group1;
                }
            }

            return new HashSet<List<PolyCircle>>(groups.Values).ToList();
        }
    }
}

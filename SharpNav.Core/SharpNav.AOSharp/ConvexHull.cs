﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public class ConvexHull
    {
        public List<Vector3> _verts;
        private List<Vector3> _cuts;

        public ConvexHull(IEnumerable<Vector3> vertices)
        {
            _verts = GetConvexHull(vertices.ToList());
            _cuts = new List<Vector3>();
        }

        public void Draw(Vector3 color)
        {
            DebugUtils.DrawOutline(_verts, color);

            foreach (var cut in _cuts)
            {
                Debug.DrawSphere(cut + Vector3.Up, 0.15f, DebuggingColor.LightBlue);
            }
        }

        public bool Cut(Vector3 startPos, Vector3 endPos, out List<Vector3> intersectPoints)
        {
            List<int> intersectIndices = new List<int>(2);
            intersectPoints = new List<Vector3>(2);

            for (int x = 0; x < _verts.Count - 1; x++)
            {
                if (!GeometryUtils.Intersects(_verts[x], _verts[x + 1], startPos, endPos, out var intersectPoint))
                    continue;

                intersectIndices.Add(x + 1);
                intersectPoints.Add(intersectPoint);
                _cuts.Add(intersectPoint);

                if (intersectIndices.Count == 2)
                    break;
            }

            if (intersectIndices.Count > 0)
            {
                for (int i = 0; i < intersectIndices.Count; i++)
                {
                    _verts.Insert(intersectIndices[i] + i, intersectPoints[i]);
                }
            }

            return intersectIndices.Count > 0;
        }

        public bool Intersects(Vector3 startPos, Vector3 endPos, out List<Vector3> intersectPoints)
        {
            intersectPoints = new List<Vector3>();

            for (int x = 0; x < _verts.Count - 1; x++)
            {
                if (!GeometryUtils.Intersects(_verts[x], _verts[x + 1], startPos, endPos, out var intersectPoint))
                    continue;

                intersectPoints.Add(intersectPoint);

                if (intersectPoints.Count() == 2)
                    return true;
            }

            return intersectPoints.Count() != 0;
        }

        public bool IsPointContained(Vector3 point, float epsilon = 0.0001f)
        {
            int intersections = 0;
            int count = _verts.Count;

            for (int i = 0; i < count; i++)
            {
                Vector3 v1 = _verts[i];
                Vector3 v2 = _verts[(i + 1) % count];

                // Ensure we only work with the XZ plane
                float x1 = v1.X;
                float z1 = v1.Z;
                float x2 = v2.X;
                float z2 = v2.Z;
                float px = point.X;
                float pz = point.Z;

                // Check if the point is exactly on a vertex or edge (with epsilon consideration)
                if (Math.Abs(px - x1) < epsilon && Math.Abs(pz - z1) < epsilon ||
                    Math.Abs(px - x2) < epsilon && Math.Abs(pz - z2) < epsilon)
                {
                    return true;
                }

                // Ray-casting intersection check
                bool isIntersecting = ((z1 > pz) != (z2 > pz)) &&
                                      (px < (x2 - x1) * (pz - z1) / (z2 - z1 + epsilon) + x1);

                if (isIntersecting)
                {
                    intersections++;
                }
            }

            // If the number of intersections is odd, the point is inside the hull
            return (intersections % 2) != 0;
        }

        public List<Vector3> FindQuickestPath(Vector3 start, Vector3 end)
        {
            // Find the closest points on the hull to the start and end points
            int startIndex = FindClosestPointIndex(_verts, start);
            int endIndex = FindClosestPointIndex(_verts, end);

            // Calculate the clockwise path
            List<Vector3> clockwisePath = new List<Vector3>();
            float clockwiseDistance = 0f;
            for (int i = startIndex; i != endIndex; i = (i + 1) % _verts.Count)
            {
                clockwisePath.Add(_verts[i]);
                if (i != startIndex)
                {
                    clockwiseDistance += Vector3.Distance(_verts[i], _verts[(i + 1) % _verts.Count]);
                }
            }
            clockwisePath.Add(_verts[endIndex]);
            clockwiseDistance += Vector3.Distance(_verts[startIndex], _verts[endIndex]);

            // Calculate the counterclockwise path
            List<Vector3> counterclockwisePath = new List<Vector3>();
            float counterclockwiseDistance = 0f;
            for (int i = startIndex; i != endIndex; i = (i - 1 + _verts.Count) % _verts.Count)
            {
                counterclockwisePath.Add(_verts[i]);
                if (i != startIndex)
                {
                    counterclockwiseDistance += Vector3.Distance(_verts[i], _verts[(i - 1 + _verts.Count) % _verts.Count]);
                }
            }
            counterclockwisePath.Add(_verts[endIndex]);
            counterclockwiseDistance += Vector3.Distance(_verts[startIndex], _verts[endIndex]);

            clockwisePath[0] = start;
            counterclockwisePath[0] = start;
            clockwisePath[clockwisePath.Count()-1] = end;
            counterclockwisePath[counterclockwisePath.Count() - 1] = end;
            // Choose the shorter path
            return clockwiseDistance <= counterclockwiseDistance ? clockwisePath : counterclockwisePath;
        }

        private int FindClosestPointIndex(List<Vector3> hull, Vector3 point)
        {
            int closestIndex = 0;
            float closestDistance = Vector3.Distance(hull[0], point);

            for (int i = 1; i < hull.Count; i++)
            {
                float distance = Vector3.Distance(hull[i], point);
                if (distance < closestDistance)
                {
                    closestIndex = i;
                    closestDistance = distance;
                }
            }

            return closestIndex;
        }

        private List<Vector3> GetConvexHull(List<Vector3> points)
        {
            if (points == null)
                return null;

            if (points.Count() <= 1)
                return points;

            int n = points.Count(), k = 0;
            List<Vector3> H = new List<Vector3>(new Vector3[2 * n]);

            points.Sort((a, b) => a.X == b.X ? a.Y.CompareTo(b.Y) : a.X.CompareTo(b.X));

            for (int i = 0; i < n; ++i)
            {
                while (k >= 2 && MathUtils.Cross(H[k - 2], H[k - 1], points[i]) <= 0)
                    k--;
                H[k++] = points[i];
            }

            for (int i = n - 2, t = k + 1; i >= 0; i--)
            {
                while (k >= t && MathUtils.Cross(H[k - 2], H[k - 1], points[i]) <= 0)
                    k--;
                H[k++] = points[i];
            }

            return H.Take(k).ToList();
        }
    }

    public struct HullCutRef
    {
        public ConvexHull Hull;
        public Vector3 Cut;

        public HullCutRef(ConvexHull hull, Vector3 cut)
        {
            Hull = hull;
            Cut = cut;
        }
    }

}

﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using SharpNav.Pathfinding;
using SmokeLounge.AOtomation.Messaging.Messages;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;

namespace AOSharp.Pathfinding
{
    public class SPathManager
    {
        private static List<SPath> _paths;

        public static IReadOnlyCollection<SPath> Paths => _paths;
        private static bool _loaded = false;

        internal static void Set()
        {
            if (_loaded)
                return;

            _paths = new List<SPath>();

            Game.OnUpdate += OnUpdate;

            _loaded = true;
        }

        private static void OnUpdate(object sender, float e)
        {
            foreach (var path in _paths)
            {
                if (path == null || path.Waypoints == null || path.Waypoints.Count == 0)
                    continue;

                if (!path.ShouldDraw)
                    continue;

                if (path.PlayfieldId != Playfield.ModelIdentity.Instance)
                    continue;

                Draw(path);
            }
        }

        internal static void Register(SPath path)
        {
            if (!_loaded)
            {
                Set();
                _loaded = true;
            }

            if (_paths.Contains(path))
            {
                Chat.WriteLine("Prevented from loading same SPath twice", ChatColor.Red);
                return;
            }

            _paths.Add(path);
        }

        internal static void Remove(SPath path)
        {
            _paths.Remove(path);
        }

        private static void Draw(SPath sPath)
        {
            var interpolatedPoints = sPath.GetWaypoints(out var newPoints);
            var pts = newPoints.OrderBy(x => Vector3.Distance(DynelManager.LocalPlayer.Position, x));
            var closestPt = pts.FirstOrDefault();

            foreach (var point in pts)
            {
                Debug.DrawSphere(point, 0.25f, point == closestPt ? DebuggingColor.Green : DebuggingColor.White);

                if (sPath.SelectedPoints.Contains(point))
                    Debug.DrawSphere(point, 0.1f, DebuggingColor.Purple);
            }

            float arrowheadLength = 0.2f;
            var arrowDir = sPath.IsReversed ? -1 : 1;

            for (int i = 0; i < interpolatedPoints.Count - 1; i++)
            {
                Debug.DrawLine(interpolatedPoints[i], interpolatedPoints[i + 1], DebuggingColor.Yellow);

                var dir = Vector3.Normalize(interpolatedPoints[i + 1] - interpolatedPoints[i])* arrowDir;
                var arrowPos = dir * arrowheadLength;

                Vector3 perpendicular1 = Vector3.Cross(dir, Vector3.Up).Normalize();

                Vector3 arrowPos1 = interpolatedPoints[i] - arrowPos + perpendicular1 * arrowheadLength;
                Vector3 arrowPos2 = interpolatedPoints[i] - arrowPos - perpendicular1 * arrowheadLength;

                Debug.DrawLine(interpolatedPoints[i], arrowPos1, DebuggingColor.Yellow);
                Debug.DrawLine(interpolatedPoints[i], arrowPos2, DebuggingColor.Yellow);
            }
        }
    }
}
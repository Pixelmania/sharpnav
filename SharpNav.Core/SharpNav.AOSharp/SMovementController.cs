﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System;
using Vector3 = AOSharp.Common.GameData.Vector3;
using SmokeLounge.AOtomation.Messaging.Messages.N3Messages;
using AOSharp.Core.UI;
using SharpNav;
using AOSharp.Common.Unmanaged.Imports;
using SmokeLounge.AOtomation.Messaging.GameData;
using System.Collections.Concurrent;
using AOSharp.Core.Misc;
using SharpNav.Pathfinding;
using static SharpNav.NavMeshQuery;
using SmokeLounge.AOtomation.Messaging.Messages;

namespace AOSharp.Pathfinding
{
    public class SMovementController
    {
        public static SNavAgent NavAgent;
        public static Action<Vector3> DestinationReached;
        public static Action<Vector3> OnRubberband;
        public static Action<Vector3, Vector3> Stuck;
        public static Action<AgentNavMeshState> AgentStateChange;
        public static NavMeshGenerationSettings AutoBakeSettings;

        internal static Vector3 Extents => _settings.PathSettings.Extents;
        public static bool AutoSetActivePathOnRubberband;
        private static SMovementControllerSettings _settings;
        private static AutoResetInterval _rotUpdate;
        private static AutoResetInterval _movementUpdate;
        private static AutoResetInterval _unstuckCheck;
        private static float _lastDist = 0f;
        private static ConcurrentQueue<MovementAction> _movementActionQueue = new ConcurrentQueue<MovementAction>();
        private static Vector3 _unstuckDir = new Vector3(0, 1.667, 1);
        private static StraightPathVertex _currentWaypoint;
        private static bool _loaded = false;
        private static Func<int, bool, bool> _shouldAutoBake;
        private static Func<int, bool, bool> _shouldAutoLoad;
        private static string _navmeshLoadPath;
        private static string _navmeshBakePath;
        private static bool _loadAfterBaking;
        private static AgentNavMeshState _agentState;
        private static StuckLogicDelegate _onStuckLogic = DefaultStuckLogic;
        private static SoftLinkHandle _softLinkHandle;

        public delegate void StuckLogicDelegate();

        public static bool IsLoaded()
        {
            return _loaded;
        }

        public static void Set(SMovementControllerSettings settings)
        {
            InternalSet(settings);
        }

        public static void AutoBakeNavmeshes(string folderPath, bool loadAfterBaking = true, Func<int, bool, bool> shouldAutoBake = null, NavMeshGenerationSettings bakeSettings = null)
        {
            _navmeshBakePath = folderPath;
            _loadAfterBaking = loadAfterBaking;
            AutoBakeSettings = bakeSettings == null ? NavMeshGenerationSettings.MediumDensity : bakeSettings;
            _shouldAutoBake = shouldAutoBake == null ? (int x, bool y) => true : shouldAutoBake;
            AutoBakeNavmesh();
        }

        public static void AutoLoadNavmeshes(string folderPath, Func<int, bool, bool> shouldAutoLoad = null)
        {
            _navmeshLoadPath = folderPath;
            _shouldAutoLoad = shouldAutoLoad == null ? (int x, bool y) => true : shouldAutoLoad;
            AutoLoadNavmesh();
        }

        private static void AutoBakeNavmesh()
        {
            if (_navmeshBakePath == null)
                return;

            var path = $"{_navmeshBakePath}\\{Playfield.ModelIdentity.Instance}.nav";

            if (System.IO.File.Exists(path) || !_shouldAutoBake(Playfield.ModelIdentity.Instance, Playfield.IsDungeon))
                return;

            SNavMeshGenerator.GenerateAsync(AutoBakeSettings).ContinueWith(autoBake =>
            {
                if (autoBake.Result == null)
                    return;

                if (_loadAfterBaking)
                    LoadNavmesh(autoBake.Result);

                SNavMeshSerializer.SaveToFile(autoBake.Result, path);
            });
        }

        private static void AutoLoadNavmesh()
        {
            if (_navmeshLoadPath == null)
                return;

            var path = $"{_navmeshLoadPath}\\{Playfield.ModelIdentity.Instance}.nav";

            if (!System.IO.File.Exists(path) || !_shouldAutoLoad(Playfield.ModelIdentity.Instance, Playfield.IsDungeon))
                return;

            if (!SNavMeshSerializer.LoadFromFile(path, out NavMesh navMesh))
            {
                Chat.WriteLine("Tried to load navmesh on zone but failed (could not be deserialized)");
                return;
            }

            if (InstanceIsNull())
                return;

            LoadNavmesh(navMesh);
        }

        public static void Set()
        {
            InternalSet(new SMovementControllerSettings());
        }

        public static void ToggleNavMeshDraw()
        {
            if (InstanceIsNull())
                return;

            _settings.NavMeshSettings.DrawNavMesh = !_settings.NavMeshSettings.DrawNavMesh;
        }

        public static void TogglePathDraw()
        {
            if (InstanceIsNull())
                return;

            _settings.PathSettings.DrawPath = !_settings.PathSettings.DrawPath;
        }

        public static void Follow(Identity identity)
        {
            if (InstanceIsNull())
                return;

            NavAgent.FollowTarget(identity);
        }

        public static void Halt()
        {
            if (InstanceIsNull())
                return;

            NavAgent.Stop();
        }


        public static bool SetNavDestination(Vector3 destination, bool ignoreIfAlreadyNavigating = false)
        {
            return SetNavDestination(destination, PathingType.Shortest, ignoreIfAlreadyNavigating);
        }

        /// <summary>
        /// Sets nav destination with obstacle avoidance. If navPolyCheckOnObstacles is set to true, 
        /// every obstacle area will ensure that all points are contained within the nav mesh
        /// at the sacrifice of potentially stepping in within the obstacle radius
        /// </summary>
        public static bool SetNavDestination(Vector3 destination, IEnumerable<Dynel> dynelsToAvoid, float avoidRadius, bool navPolyCheckOnObstacles, bool ignoreIfAlreadyNavigating = false)
        {
            return SetNavDestination(destination, dynelsToAvoid, avoidRadius, navPolyCheckOnObstacles, false, ignoreIfAlreadyNavigating);
        }

        public static bool SetNavDestinationDebug(Vector3 destination, IEnumerable<Dynel> dynelsToAvoid, float avoidRadius, bool navPolyCheckOnObstacles, bool ignoreIfAlreadyNavigating = false)
        {
            return SetNavDestination(destination, dynelsToAvoid, avoidRadius, navPolyCheckOnObstacles, true, ignoreIfAlreadyNavigating);
        }

        private static bool SetNavDestination(Vector3 destination, IEnumerable<Dynel> dynelsToAvoid, float avoidRadius, bool navPolyCheckOnObstacles, bool debugMode, bool ignoreIfAlreadyNavigating = false)
        {
            if (InstanceIsNull())
                return false;

            if (NavAgent.IsNavigating && ignoreIfAlreadyNavigating)
                return false;

            NavAgent.SetNavDestination(destination, dynelsToAvoid, avoidRadius, navPolyCheckOnObstacles, debugMode);

            return true;
        }


        public static bool SetNavDestination(Vector3 destination, PathingType pathType, bool ignoreIfAlreadyNavigating = false)
        {
            if (InstanceIsNull())
                return false;

            if (NavAgent.IsNavigating && ignoreIfAlreadyNavigating)
                return false;

            switch (pathType)
            {
                case PathingType.Interpolated:
                    NavAgent.SetSmoothNavDestination(destination, 1.25f);
                    break;
                case PathingType.Shortest:
                    NavAgent.SetNavDestination(destination);
                    break;
            }

            return true;
        }

        internal static List<StraightPathVertex> GenerateNavPath(Vector3 destination)
        {
            if (InstanceIsNull())
                return new List<StraightPathVertex>();

            return NavAgent.GenerateNavPath(DynelManager.LocalPlayer.Position, destination);
        }

        public static Vector3 GetRandomPoint()
        {
            if (InstanceIsNull())
                return Vector3.Zero;

            return NavAgent.GetRandomPoint();
        }

        public static Vector3 GetClosestNavPoint(Vector3 position)
        {
            if (InstanceIsNull())
                return Vector3.Zero;

            if (NavAgent.FindNearestNavPoint(position, Extents, out NavPoint point))
                return point.Position.ToVector3();

            return Vector3.Zero;
        }

        public static bool GetClosestNavPoint(Vector3 extents, out Vector3 position)
        {
            position = Vector3.Zero;

            if (InstanceIsNull())
                return false;

            if (!NavAgent.FindNearestNavPoint(DynelManager.LocalPlayer.Position, extents, out NavPoint point))
                return false;

            position = point.Position.ToVector3();

            return true;
        }

        /// <summary>
        /// Generates nav waypoints from the agents current position to the given end position
        /// </summary>
        public static bool GenerateNavPath(Vector3 endPos, out List<StraightPathVertex> waypoints, out float totalDistance)
        {
            totalDistance = 0;
            waypoints = new List<StraightPathVertex>();

            if (InstanceIsNull())
                return false;

            var navPath = NavAgent.GenerateNavPath(DynelManager.LocalPlayer.Position, endPos);

            if (navPath.Count() == 0)
                return false;

            waypoints = navPath;
            navPath.Insert(0, DynelManager.LocalPlayer.Position.ToPathVertex());
            totalDistance = MathUtils.GetDistance(navPath);

            return true;
        }

        public static Vector3 GetClosestNavPoint()
        {
            return GetClosestNavPoint(DynelManager.LocalPlayer.Position);
        }

        public static void AppendNavDestination(Vector3 destination)
        {
            AppendNavDestination(DynelManager.LocalPlayer.Position, destination);
        }

        public static void AppendNavDestination(Vector3 startPos, Vector3 destination)
        {
            if (InstanceIsNull())
                return;

            NavAgent.AppendNavDestination(startPos, destination);
        }

        public static bool IsNavigating()
        {
            if (InstanceIsNull())
                return false;

            return NavAgent.IsNavigating;
        }

        /// <summary>
        /// Loads a navmesh, set forceLoad to true to disable checking if current playfield has a navmesh already loaded
        /// </summary>
        public static void LoadNavmesh(NavMesh navMesh, bool forceLoad = false)
        {
            if (InstanceIsNull())
                return;

            NavAgent.SetNavmesh(navMesh, forceLoad);
        }

        public static void UnloadNavmesh()
        {
            if (InstanceIsNull())
                return;

            NavAgent?.DeleteNavmesh();
        }

        public static void SetMovement(MovementAction action)
        {
            _movementActionQueue.Enqueue(action);
        }

        public static bool SetPath(SPath sPath, Vector3 destination, bool startAtClosestPoint = false, float cutoffDistance = 0.5f)
        {
            if (InstanceIsNull())
                return false;

            if (!GetPoints(sPath, startAtClosestPoint, false, out var pointsForward))
                return false;

            var shiftedForw = ShiftAndCutByPoint(pointsForward.ToList(), DynelManager.LocalPlayer.Position, destination);

            if (!GetPoints(sPath, startAtClosestPoint, true, out var pointsBackward))
                return false;

            var shiftedBack = ShiftAndCutByPoint(pointsBackward.ToList(), DynelManager.LocalPlayer.Position, destination);

            if (shiftedBack.Count == 0 && shiftedForw.Count == 0 || shiftedBack.Count == 1 || shiftedForw.Count == 1)
                return false;

            var finalPoints = shiftedBack.Count > shiftedForw.Count ? shiftedForw : shiftedBack;

            CullUnneededWaypoints2D(finalPoints, cutoffDistance);

            return SetWaypoints(finalPoints);
        }

        private static void CullUnneededWaypoints(List<Vector3> path, float cutoffDistance = 0.5f)
        {
            if (path.Count < 2)
                return;

            for (int i = 0; i < path.Count - 1; i++)
            {
                var pointOnFirstSegment = MathUtils.ClosestPointOnLineSegment(path[i], path[i + 1], DynelManager.LocalPlayer.Position);

                if (DynelManager.LocalPlayer.Position.Distance2DFrom(pointOnFirstSegment) < cutoffDistance)
                    path.RemoveAt(i--);
            }
        }

        private static void CullUnneededWaypoints2D(List<Vector3> path, float cutoffDistance = 0.5f)
        {
            if (path.Count < 2)
                return;

            for (int i = 0; i < path.Count - 1; i++)
            {
                var pointOnFirstSegment = MathUtils.ClosestPointOnLineSegment2D(path[i], path[i + 1], DynelManager.LocalPlayer.Position);

                if (DynelManager.LocalPlayer.Position.Distance2DFrom(pointOnFirstSegment) < cutoffDistance)
                    path.RemoveAt(i--);
            }
        }

        private static List<Vector3> ShiftAndCutByPoint(List<Vector3> list, Vector3 playerPos, Vector3 targetPos)
        {
            var playerIndex = list.IndexOf(list.OrderBy(x => Vector3.Distance(playerPos, x)).FirstOrDefault());
            List<Vector3> shiftedListBack = new List<Vector3>();

            shiftedListBack.AddRange(list.GetRange(playerIndex, list.Count - playerIndex));
            shiftedListBack.AddRange(list.GetRange(0, playerIndex));

            var targetIndex = shiftedListBack.IndexOf(shiftedListBack.OrderBy(x => Vector3.Distance(targetPos, x)).FirstOrDefault());

            shiftedListBack.RemoveRange(targetIndex, shiftedListBack.Count - targetIndex);

            return shiftedListBack;
        }

        public static bool SetPath(SPath sPath, bool startAtClosestPoint = false, float cutoffDistance = 0.5f)
        {
            if (InstanceIsNull())
                return false;

            if (!GetPoints(sPath, startAtClosestPoint, sPath.IsReversed, out var points))
                return false;

            CullUnneededWaypoints2D(points, cutoffDistance);

            return SetWaypoints(points);
        }

        public static bool SetDestination(Vector3 destination, bool ignoreIfAlreadyNavigating = false)
        {
            if (NavAgent.IsNavigating && ignoreIfAlreadyNavigating)
                return false;

            SetWaypoints(new List<Vector3> { destination });

            return true;
        }

        public static bool AppendPath(SPath sPath, bool startAtClosestPoint, bool reverseDirection)
        {
            if (!GetPoints(sPath, startAtClosestPoint, reverseDirection, out var points))
                return false;

            return AppendWaypoints(points);
        }

        private static bool GetPoints(SPath sPath, bool startAtClosestPoint, bool reverseDirection, out List<Vector3> points) 
        {
            points = new List<Vector3>();

            if (sPath == null || sPath.Waypoints == null || sPath.Waypoints.Count == 0)
                return false;

            sPath.IsLocked = true;

            points = sPath.GetWaypoints(out _);

            if (reverseDirection)
                points.Reverse();

            if (startAtClosestPoint)
            {
                var closestPoint = points.OrderBy(x => Vector3.Distance(DynelManager.LocalPlayer.Position, x)).FirstOrDefault();
                var index = points.IndexOf(closestPoint);
                points.RemoveRange(0, index);
            }

            return true;
        }

        internal static bool SetWaypoints(List<Vector3> waypoints)
        {
            List<StraightPathVertex> vertexPoints = new List<StraightPathVertex>();

            foreach (var waypoint in waypoints)
                vertexPoints.Add(waypoint.ToPathVertex());

            return SetWaypoints(vertexPoints);
        }

        internal static bool SetWaypoints(List<StraightPathVertex> waypoints)
        {
            if (waypoints.Count() == 0 || waypoints == null)
                return false;

            NavAgent.Path.Clear();
            _lastDist = 0;

            return AppendWaypoints(waypoints);
        }

        internal static bool AppendWaypoints(List<Vector3> waypoints)
        {
            List<StraightPathVertex> vertexPoints = new List<StraightPathVertex>();

            foreach (var waypoint in waypoints)
                vertexPoints.Add(waypoint.ToPathVertex());

            return AppendWaypoints(vertexPoints);
        }

        internal static bool AppendWaypoints(List<StraightPathVertex> waypoints)
        {
            if (waypoints == null || waypoints.Count() == 0)
                return false;

            Path path = new Path(waypoints, _settings.PathSettings.PathRadius, _settings.PathSettings.PathRadius);

            if (path.Waypoints.Count == 0)
                return false;

            NavAgent.Path.Enqueue(path);
            return true;
        }

        private static void OnStuck(Vector3 destination)
        {
            Stuck?.Invoke(DynelManager.LocalPlayer.Position, destination);
            _onStuckLogic?.Invoke();
        }

        private static void DefaultStuckLogic()
        {
            DynelManager.LocalPlayer.Position += DynelManager.LocalPlayer.Rotation * _unstuckDir * 0.5f;
        }

        public static void SetStuckLogic(StuckLogicDelegate newLogic)
        {
            _onStuckLogic = newLogic;
        }

        private static bool InstanceIsNull()
        {
            if (NavAgent != null)
                return false;

            Chat.WriteLine("SMovementController not set. (SMovementController.Set() not executed?)", ChatColor.Red);

            return true;
        }

        private static void Update(object sender, float deltaTime)
        {
            try
            {
                if (NavAgent == null)
                    return;

                if (NavAgent.HasPathfinder && _settings.NavMeshSettings.DrawNavMesh)
                    DebugDrawUpdate();

                while (_movementActionQueue.TryDequeue(out MovementAction action))
                    ChangeMovement(action);

                if (NavAgent.IsNavigating)
                    PathUpdate(deltaTime);
            }
            catch (Exception e)
            {
                Chat.WriteLine($"This shouldn't happen pls report (SMovementController): {e.Message}");
            }
        }

        private static void PathUpdate(float deltaTime)
        {
            try
            {
                Path currentPath = NavAgent.CurrentPath;

                if (!NavAgent.IsMoving)
                {
                    _unstuckCheck.Reset();
                    SetMovement(MovementAction.ForwardStart);
                }

                if (!currentPath.Initialized)
                    currentPath.OnPathStart();

                switch (currentPath.GetNextWaypoint(out StraightPathVertex waypoint))
                {
                    case PathingPhase.EndReached:
                        Path currPath = NavAgent.Path.Dequeue();
                        currPath.OnPathFinished();
                        _lastDist = 0;

                        if (!NavAgent.IsNavigating && currPath.StopAtDest)
                        {
                            DestinationReached?.Invoke(_currentWaypoint.Position);
                            SetMovement(MovementAction.ForwardStop);
                        }

                        return;
                    case PathingPhase.Traversing:

                        break;
                    case PathingPhase.NewPointReached:
                        _currentWaypoint = waypoint;
                        _softLinkHandle.Update(_currentWaypoint);
                        break;

                }

                if (_unstuckCheck.Elapsed && NavAgent.IsMoving)
                    OnStuckUpdate(currentPath);

                if (_rotUpdate.Elapsed)
                    NavAgent.LerpTowards(waypoint.Position, _settings.PathSettings.MinRotSpeed, _settings.PathSettings.MaxRotSpeed, deltaTime);

                if (_movementUpdate.Elapsed)
                    SetMovement(MovementAction.Update);

                if (_settings.PathSettings.DrawPath && NavAgent.IsNavigating)
                    currentPath.Draw();
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
            }
        }

        private static void OnStuckUpdate(Path currentPath)
        {
            float currentDist = NavAgent.CurrentPath.Length();

            if (_lastDist != 0 && currentDist >= _lastDist)
            {
                if (currentPath.PeekLastWaypoint(out StraightPathVertex peekPoint))
                    OnStuck(peekPoint.Position);
            }

            _lastDist = currentDist;
        }


        private static void DebugDrawUpdate()
        {
            Vector3 meshColor = DebuggingColor.Green;
            Vector3 cylinderColor = DebuggingColor.Red;

            foreach (SoftLinkConnection connection in NavAgent.NavMesh?.SoftLinkConnections)
            {
                if (Vector3.Distance(connection.Link0.Position.ToVector3(), DynelManager.LocalPlayer.Position) > _settings.NavMeshSettings.DrawDistance * 3)
                    continue;

                connection.Draw();
            }

            foreach (NavTile tile in NavAgent.NavMesh?.Tiles)
            {
                foreach (var connection in tile?.OffMeshConnections)
                {
                    if (Vector3.Distance(DynelManager.LocalPlayer.Position, connection.Pos0.ToVector3()) > _settings.NavMeshSettings.DrawDistance)
                        continue;

                    Debug.DrawLine(connection.Pos0.ToVector3(), connection.Pos1.ToVector3(), DebuggingColor.Yellow);
                }
            }

            if (NavAgent.FindPolys(_settings.NavMeshSettings.DrawDistance, out Poly[] polies) == AgentNavMeshState.InPolygon)
            {
                for (int i = 0; i < polies.Length; i++)
                {
                    if (polies[i].Area != Area.Null)
                        polies[i].Draw(meshColor);
                }
            }

            AgentNavMeshState agentState = NavAgent.IsOnNavPoly(_settings.PathSettings.PathRadius, out Poly poly);

            if (_agentState != agentState)
            {
                _agentState = agentState;
                AgentStateChange?.Invoke(_agentState);
            }

            switch (agentState)
            {
                case AgentNavMeshState.InPolygon:
                    poly.Draw(DebuggingColor.LightBlue, Vector3.Up * 0.01f);
                    cylinderColor = DebuggingColor.Green;
                    break;
                case AgentNavMeshState.OutPolygon:
                    cylinderColor = new Vector3(1f, 0.5f, 0f);
                    break;
                case AgentNavMeshState.OutNavMesh:
                    cylinderColor = DebuggingColor.Red;
                    break;
            }

            Vector3 rayOrigin = DynelManager.LocalPlayer.Position;
            Vector3 rayTarget = DynelManager.LocalPlayer.Position;
            rayTarget.Y = 0;

            if (!Playfield.Raycast(rayOrigin, rayTarget, out Vector3 hitPos, out _))
                hitPos = rayOrigin;

            SDebug.DrawCylinder(hitPos, _settings.PathSettings.PathRadius, (float)DynelManager.LocalPlayer.GetStat(Stat.Scale) / 100, cylinderColor);
        }

        private static void InternalSet(SMovementControllerSettings settings)
        {
            AutoSetActivePathOnRubberband = true;
            NavAgent = new SNavAgent();

            _softLinkHandle = new SoftLinkHandle();
            _settings = settings;
            _rotUpdate = new AutoResetInterval(_settings.PathSettings.RotUpdate);
            _movementUpdate = new AutoResetInterval(_settings.PathSettings.MovementUpdate);
            _unstuckCheck = new AutoResetInterval(_settings.PathSettings.UnstuckUpdate);
           
            Chat.WriteLine("SMovementController was set.", ChatColor.Green);

            if (!_loaded)
            {
                Game.OnUpdate += Update;
                Game.PlayfieldInit += OnPlayfieldInit;
                Game.TeleportStarted += OnTeleportStarted;
                Network.N3MessageReceived += OnSetPosReceived;
                SPathManager.Set();
                _loaded = true;
            }
        }

        private static void OnSetPosReceived(object sender, N3Message e)
        {
            if (!(e is SetPosMessage setPosMsg))
                return;

            if (setPosMsg.Identity != DynelManager.LocalPlayer.Identity)
                return;

            if (!AutoSetActivePathOnRubberband)
                return;

            DynelManager.LocalPlayer.Position = setPosMsg.Position;
            
            if (NavAgent.NavMesh != null)
            {
                if (NavAgent.CurrentPath.PeekLastWaypoint(out StraightPathVertex lastWaypoint))
                    SetNavDestination(lastWaypoint.Position);
            }

            OnRubberband?.Invoke(setPosMsg.Position);
        }

        private static void OnTeleportStarted(object sender, EventArgs e)
        {
            if (NavAgent.IsNavigating)
                Halt();
        }

        private static void OnPlayfieldInit(object sender, uint e)
        {
            if (NavAgent.TryGetNavmeshPfId(out int pfId) && pfId == Playfield.ModelIdentity.Instance)
                return;

            NavAgent.ResetPathfinder();
            AutoLoadNavmesh();
            AutoBakeNavmesh();
        }

        //Must be called from game loop!
        private static void ChangeMovement(MovementAction action)
        {
            if (action == MovementAction.LeaveSit)
            {
                Network.Send(new CharacterActionMessage()
                {
                    Action = CharacterActionType.StandUp
                });
            }
            else
            {
                IntPtr pEngine = N3Engine_t.GetInstance();

                if (pEngine == IntPtr.Zero)
                    return;

                N3EngineClientAnarchy_t.MovementChanged(pEngine, action, 0, 0, true);
            }
        }
    }
}

public enum PathingType
{
    Shortest,
    Interpolated
}

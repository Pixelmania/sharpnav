﻿using AOSharp.Core;
using SharpNav.Pathfinding;
using System;
using System.Collections.Generic;
using System.Numerics;
using AVector3 = AOSharp.Common.GameData.Vector3;
using SVector3 = SharpNav.Geometry.Vector3;

namespace AOSharp.Pathfinding
{
    public class SoftLinkConnection
    {
        public List<OffMeshConnection> Connections = new List<OffMeshConnection>();
        public SoftLink Link0;
        public SoftLink Link1;
        internal OffMeshConnectionFlags Flags;

        public SoftLinkConnection(SoftLink link0, SoftLink link1, OffMeshConnectionFlags flag = OffMeshConnectionFlags.None)
        {
            Link0 = link0;
            Link1 = link1;
            Flags = flag;
            Connections.Add(new OffMeshConnection(link0, link1));

            if (flag == OffMeshConnectionFlags.Bidirectional)
                Connections.Add(new OffMeshConnection(link1, link0));
        }

        public void Draw()
        {
            var pos = new List<AVector3> { Link0.Position.ToVector3(), Link1.Position.ToVector3() };
            var color = Link0 is JumpLink ? DebuggingColor.LightBlue : Link1 is StandOnDynelLink ? DebuggingColor.Purple : DebuggingColor.Red;

            Debug.DrawSphere(pos[0], 0.25f, color);
            Debug.DrawSphere(pos[1], 0.25f, color);

            pos.Insert(1, ((Link0.Position + Link1.Position) / 2).ToVector3() + AVector3.Up * 3f);
            var inter = MathUtils.InterpolateXYZ(pos, 0.5f);

            for (int i = 0; i < inter.Count - 1; i = i + 3)
            {
                Debug.DrawLine(inter[i], inter[i + 1], color);
            }
        }
    }

    public enum SoftLinkType
    {
        StandOn,
        Use,
        Jump
    }

    public class UsableDynelLink : SoftLink
    {
        public override SoftLinkType SoftLinkType => SoftLinkType.Use;

        public string Name;

        public UsableDynelLink(string name, AVector3 position) : base(position)
        {
            Name = name;
        }
    }

    public class StandOnDynelLink : SoftLink
    {
        public override SoftLinkType SoftLinkType => SoftLinkType.StandOn;

        public StandOnDynelLink(AVector3 position) : base(position)
        {
        }
    }

    public class JumpLink : SoftLink
    {
        public override SoftLinkType SoftLinkType => SoftLinkType.Jump;

        public JumpLink(AVector3 position) : base(position)
        {
        }
    }

    public abstract class SoftLink
    {
        public abstract SoftLinkType SoftLinkType { get; }

        public SVector3 Position;

        public SoftLink(AVector3 position)
        {
            Position = position.ToSharpNav();
        }
    }
}
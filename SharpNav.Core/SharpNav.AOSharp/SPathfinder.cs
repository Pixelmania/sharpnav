﻿using System;
using System.Collections.Generic;
using System.Linq;
using AOSharp.Common.GameData;
using SVector3 = SharpNav.Geometry.Vector3;
using AVector3 = AOSharp.Common.GameData.Vector3;
using NavmeshQuery = SharpNav.NavMeshQuery;
using SharpNav.Pathfinding;
using SharpNav;
using AOSharp.Core.UI;
using AOSharp.Core;
using static SharpNav.NavMeshQuery;
using SharpNav.Geometry;

namespace AOSharp.Pathfinding
{
    public class SPathfinder
    {
        internal NavMesh NavMesh;
        internal int PlayfieldId;
        private NavmeshQuery _query;
        private NavQueryFilter _filter;

        public SPathfinder(NavMesh navMesh, int playfieldId)
        {
            NavMesh = navMesh;
            PlayfieldId = playfieldId;
            _filter = new NavQueryFilter();
            _query = new NavmeshQuery(navMesh, 8192);
        }

        internal AgentNavMeshState IsOnNavMesh(float radius, out Poly poly)
        {
            poly = new Poly();
            AVector3 startPos = DynelManager.LocalPlayer.Position + AVector3.Up;
            AVector3 rayCastTarget = startPos;
            rayCastTarget.Y = 0;

            if (!Playfield.Raycast(startPos, rayCastTarget, out AVector3 hitPos, out _))
                return AgentNavMeshState.OutNavMesh;

            AVector3 extents = new AVector3(radius, 5f, radius);

            try
            {

                AgentNavMeshState agentState =  IsOnNavmesh(hitPos.ToSharpNav(), extents.ToSharpNav(), out SVector3[] result, out NavPolyId id);

               // Chat.WriteLine(agentState);
                switch (agentState)
                {
                    case AgentNavMeshState.OutNavMesh:
                        break;
                    case AgentNavMeshState.InPolygon:
                        poly = new Poly(result.Select(x => x.ToVector3()).ToArray(), id, Area.Default);
                        break;
                    default:
                        break;
                }

                return agentState;
            }
            catch
            {
                return AgentNavMeshState.OutNavMesh;
            }
        }

        private AgentNavMeshState IsOnNavmesh(SVector3 center, SVector3 extents, out SVector3[] result, out NavPolyId polyId)
        {
            result = null;
            List<NavPolyId> polys = new List<NavPolyId>();
            polyId = new NavPolyId();

            _query.FindNearestPoly(ref center, ref extents, out NavPoint closestPt);

            if (closestPt.Polygon == NavPolyId.Null)
                return AgentNavMeshState.OutNavMesh;

            NavMesh.TryGetTileAndPolyByRefUnsafe(closestPt.Polygon, out NavTile meshTile, out NavPoly navPoly);

            result = new SVector3[navPoly.VertCount];

            for (int v = 0; v < navPoly.VertCount; v++)
                result[v] = meshTile.Verts[navPoly.Verts[v]];

            return Containment.PointInPoly(center, result, navPoly.VertCount) ? AgentNavMeshState.InPolygon : AgentNavMeshState.OutPolygon;
        }

        internal NavPoint GetRandomPoint() => _query.FindRandomPoint();

        internal List<StraightPathVertex> GeneratePath(AVector3 start, AVector3 end)
        {
            List<AVector3> newPath = new List<AVector3>();

            if (!FindNearestPoint(start, SMovementController.Extents, out NavPoint origin) || origin.Position == new SVector3())
            {
                Chat.WriteLine("Could not find valid origin point on navmesh");
                return new List<StraightPathVertex>();
            }

            if (!FindNearestPoint(end, SMovementController.Extents, out NavPoint destination) || destination.Position == new SVector3())
            {
                Chat.WriteLine("Could not find valid destination point on navmesh");
                return new List<StraightPathVertex>();
            }

            //Chat.WriteLine($"Want path from {start} to {end}. Search Extents: {SMovementController.Extents}");
            //Chat.WriteLine($"Generating path from {origin.Position}({origin.Polygon}) to {destination.Position}({destination.Polygon})");

            SharpNav.Pathfinding.Path path = new SharpNav.Pathfinding.Path();

            if (!_query.FindPath(ref origin, ref destination, _filter, path))
                return new List<StraightPathVertex>();

            List<StraightPathVertex> straightPath = StraightenPath(start.ToSharpNav(), end.ToSharpNav(), path);

           CullUnneededWaypoints(straightPath);

            return straightPath;
        }

        private void SplitByDistance(List<AVector3> waypoints)
        {
            List<AVector3> finalPath = new List<AVector3>();
            float splittingPoint = 20f;

            for (int i = 0; i < waypoints.Count - 1; i++)
            {
                finalPath.Add(waypoints[i]);

                float distance = AVector3.Distance(waypoints[i], waypoints[i + 1]);

                if (distance >= splittingPoint)
                {
                    int numSegments = (int)Math.Ceiling(distance / splittingPoint);
                    AVector3 segmentDirection = (waypoints[i + 1] - waypoints[i]).Normalize();
                    float segmentLength = distance / numSegments;

                    for (int j = 1; j < numSegments; j++)
                    {
                        AVector3 splitPoint = waypoints[i] + segmentDirection * (j * segmentLength);
                        finalPath.Add(splitPoint);
                    }
                }
            }

            finalPath.Add(waypoints.Last());
        }

        private void CullUnneededWaypoints(List<StraightPathVertex> path)
        {
            if (path.Count < 2)
                return;

            for (int i = 0; i < path.Count - 1; i++)
            {
                if (path[i].Flags == StraightPathFlags.OffMeshConnection || path[i + 1].Flags == StraightPathFlags.OffMeshConnection)
                    continue;

                var pointOnFirstSegment = MathUtils.ClosestPointOnLineSegment(path[i].Position, path[i + 1].Position, DynelManager.LocalPlayer.Position);

                if (DynelManager.LocalPlayer.Position.Distance2DFrom(pointOnFirstSegment) < 0.5f)
                    path.RemoveAt(i--);
            }
        }

        private List<StraightPathVertex> GenerateInterpolatedPath(AVector3 startPos, AVector3 endPos, float density)
        {
            try
            {
                List<StraightPathVertex> path = GeneratePath(startPos, endPos);
                path.Insert(0, startPos.ToPathVertex());
                var interpolatedPath = MathUtils.InterpolateXYZ(path.Select(x => x.Position).ToList(), density).ToList();
                return path.Count < 2 ? path : interpolatedPath.Select(x => x.ToPathVertex()).ToList();
            }
            catch (Exception ex)
            {
                Chat.WriteLine(ex.Message);
                return new List<StraightPathVertex>();
            }
        }

        internal List<StraightPathVertex> GenerateSmoothPath(AVector3 startPos, AVector3 endPos, float density)
        {
            try
            {
                List<StraightPathVertex> initialPath = new List<StraightPathVertex>();

                foreach (var position in GenerateInterpolatedPath(startPos, endPos, density))
                {
                    if (FindNearestPoint(position.Position, SMovementController.Extents, out NavPoint point))
                        initialPath.Add(position);
                }

                return initialPath;
            }
            catch
            {
                return new List<StraightPathVertex>();
            }
        }

        private List<StraightPathVertex> StraightenPath(SVector3 start, SVector3 end, SharpNav.Pathfinding.Path path)
        {
            StraightPath straightPath = new StraightPath();

            if (!_query.FindStraightPath(start, end, path, straightPath, PathBuildFlags.AllCrossingVertices))
                throw new Exception("Failed to straighten path.");

            return straightPath.Verts;
        }

        public bool FindNearestPoint(AVector3 position, AVector3 extents, out NavPoint point)
        {
            point = new NavPoint();

            try
            {
                SVector3 startPoint = position.ToSharpNav();
                SVector3 sExtents = extents.ToSharpNav();
                _query.FindNearestPoly(ref startPoint, ref sExtents, out point);
                return true;
            }
            catch 
            {
                return false;
            }
        }

        public bool IsUsingNavmesh(TiledNavMesh navmesh)
        {
            return navmesh == NavMesh;
        }

        internal AgentNavMeshState FindPolys(int distance, out Poly[] polies)
        {
            SVector3 center = DynelManager.LocalPlayer.Position.ToSharpNav();
            SVector3 extents = new SVector3(distance, 20, distance);

            return _query.FindPolygons(ref center, ref extents, out polies);
        }
    }
}
﻿using SharpNav.Geometry;
using AOSharp.Pathfinding;
using AOSharp.Core;
using System.Collections.Generic;
using System;

public static class RoomGeoUtils
{
    public static List<Triangle3> ToTriangle3(Room room)
    {
        var tris = TerrainData.GetRoomGeometry(room);

        for (int i = 0; i < tris.Count; i++)
        {
            var tri = tris[i];
            var roomCenter = GetCenter(room);

            tri.A -= roomCenter;
            tri.B -= roomCenter;
            tri.C -= roomCenter;

            tri.A = Vector3.Rotate(tri.A, Vector3.UnitY, 360 - room.Rotation);
            tri.B = Vector3.Rotate(tri.B, Vector3.UnitY, 360 - room.Rotation);
            tri.C = Vector3.Rotate(tri.C, Vector3.UnitY, 360 - room.Rotation);

            tris[i] = tri;
        }

        return tris;
    }

    public static Vector3 GetCenter(Room room)
    {
        float centerX = (room.Rect.MinX + room.Rect.MaxX) / 2;
        float centerZ = (room.Rect.MinY + room.Rect.MaxY) / 2;

        return new Vector3(centerX, room.Position.Y, centerZ);
    }
}
﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using SharpNav.Pathfinding;
using SharpNav;

namespace AOSharp.Pathfinding
{
    public struct PolyVertexEdge
    {
        public PolyVertex V1 { get; }
        public PolyVertex V2 { get; }

        public PolyVertexEdge(PolyVertex v1, PolyVertex v2)
        {
            V1 = v1;
            V2 = v2;
        }

        public override bool Equals(object obj)
        {
            if (obj is PolyVertexEdge other)
            {
                return (V1.Equals(other.V1) && V2.Equals(other.V2)) ||
                       (V1.Equals(other.V2) && V2.Equals(other.V1));
            }
            return false;
        }

        public override int GetHashCode()
        {
            return V1.GetHashCode() ^ V2.GetHashCode();
        }
    }
}

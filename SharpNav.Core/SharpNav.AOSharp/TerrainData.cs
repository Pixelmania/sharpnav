﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Linq;
using System.Collections.Generic;
using System;
using AOSharp.Core.UI;
using SharpNav;
using AOSharp.Common.Unmanaged.DbObjects;
using System.Diagnostics;
using Mesh = AOSharp.Common.GameData.Mesh;
using STriangle3 = SharpNav.Geometry.Triangle3;
using Vector3 = AOSharp.Common.GameData.Vector3;
using System.Threading.Tasks;
using System.Collections.Concurrent;

namespace AOSharp.Pathfinding
{
    public class TerrainData
    {
        public static List<STriangle3> GetRoomGeometry(Room room)
        {
            DungeonRDBTilemap tilemap = Playfield.RDBTilemap as DungeonRDBTilemap;
            List<Mesh> meshes = room.SurfaceResource.Meshes;
            meshes.Add(DungeonTerrain.CreateMesh(room, tilemap));

            return GetTriMesh(meshes);
        }

        public static Dictionary<int, List<STriangle3>> GetPerFloorDungeonGeometry(Rect bounds)
        {
            Chat.WriteLine("Starting triangle geometry export", ChatColor.LightBlue);

            Stopwatch sw = Stopwatch.StartNew();

            if (!Playfield.IsDungeon)
            {
                throw new Exception("Must be in a dungeon!");
            }

            Dictionary<int, List<STriangle3>> perFloorMeshes = new Dictionary<int, List<STriangle3>>();
            DungeonRDBTilemap tilemap = Playfield.RDBTilemap as DungeonRDBTilemap;

            foreach (Room room in Playfield.Rooms)
            {
                if (!perFloorMeshes.ContainsKey(room.Floor))
                    perFloorMeshes.Add(room.Floor, new List<STriangle3>());

                List<Mesh> meshes = room.SurfaceResource.Meshes;
                meshes.Add(DungeonTerrain.CreateMesh(room, tilemap));

                List<STriangle3> tris = GetTriMesh(meshes);

                if (perFloorMeshes.TryGetValue(room.Floor, out var triList))
                    triList.AddRange(tris);
            }

            Chat.WriteLine($"Loaded tri mesh data {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            return perFloorMeshes;
        }


        public static List<STriangle3> GetTriGeometry(Rect bounds)
        {
            Chat.WriteLine("Starting triangle geometry export", ChatColor.LightBlue);

            Stopwatch sw = Stopwatch.StartNew();

            List<Mesh> meshes = Playfield.IsDungeon ?
                DungeonTerrain.CreateFromCurrentPlayfield() :
                Terrain.CreateFromCurrentPlayfield();

            Chat.WriteLine($"Loaded terrain mesh data. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            List<SurfaceResource> surfaces = Playfield.IsDungeon ?
                Playfield.Rooms.Select(x => x.SurfaceResource).ToList() :
                Playfield.Zones.Select(x => SurfaceResource.Get(Playfield.ModelIdentity.Instance << 16 | x.Instance)).ToList();

            Chat.WriteLine($"Loaded surface resource mesh data {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            return GetTriGeometry(bounds, sw, meshes, surfaces);
        }

        private static List<STriangle3> GetTriGeometry(Rect bounds, Stopwatch sw, List<Mesh> meshes, List<SurfaceResource> surfaces)
        {
            List<STriangle3> tris = new List<STriangle3>();

            tris.AddRange(GetTriMesh(meshes));

            Chat.WriteLine($"Converted terrain mesh data to triangle data. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            tris.AddRange(GetTriMesh(surfaces));

            Chat.WriteLine($"Converted surface resource mesh data to triangle data. {sw.ElapsedAndReset().FormatTime()}", ChatColor.LightBlue);

            RemoveBounds(sw, bounds, tris);

            return tris;
        }

        private static void RemoveBounds(Stopwatch sw, Rect bounds, List<STriangle3> tris)
        {
            Rect defaultBounds = Rect.Default;

            if (!(bounds.MinX == Rect.Default.MinX && bounds.MaxX == defaultBounds.MaxX && bounds.MinY == defaultBounds.MinY && bounds.MaxY == defaultBounds.MaxY))
            {
                ConcurrentBag<STriangle3> trianglesToRemove = new ConcurrentBag<STriangle3>();

                Parallel.ForEach(tris.ToList(), tri =>
                {
                    if (!bounds.Contains(tri.A.ToVector3()) ||
                        !bounds.Contains(tri.B.ToVector3()) ||
                        !bounds.Contains(tri.C.ToVector3()))
                    {
                        trianglesToRemove.Add(tri);
                    }
                });

                foreach (var triToRemove in trianglesToRemove)
                {
                    tris.Remove(triToRemove);
                }


                Chat.WriteLine($"Removing triangles outside of given bounds {bounds}. {sw.ElapsedAndReset().FormatTime()}", ChatColor.Green);
            }
        }

        public static List<STriangle3> GetTriGeometry(Room room)
        {
            Mesh groundMesh = DungeonTerrain.CreateMesh(room, (DungeonRDBTilemap)Playfield.RDBTilemap);

            SurfaceResource surface = room.SurfaceResource;

            List<STriangle3> tris = new List<STriangle3>();

            tris.AddRange(GetTriMesh(groundMesh));
            tris.AddRange(GetTriMesh(surface));

            return tris;
        }

        private static List<STriangle3> GetTriMesh(Mesh mesh)
        {
            return GetTriMesh(new List<Mesh>() { mesh });
        }

        private static List<STriangle3> GetTriMesh(SurfaceResource surface)
        {
            return GetTriMesh(new List<SurfaceResource>() { surface });
        }

        private static List<STriangle3> GetTriMesh(List<SurfaceResource> surfaces)
        {
            List<STriangle3> tris = new List<STriangle3>();

            foreach (var surface in surfaces)
            {
                if (surface == null)
                    continue;

                tris.AddRange(GetTriMesh(surface.Meshes));
            }

            return tris;
        }

        private static List<STriangle3> GetTriMesh(List<Mesh> meshes)
        {
            List<STriangle3> tris = new List<STriangle3>();

            foreach (var mesh in meshes)
            {
                try
                {
                    for (int i = 0; i < mesh.Triangles.Count / 3; i++)
                    {
                        int num = i * 3;
                        int index = mesh.Triangles[num];
                        int index2 = mesh.Triangles[num + 1];
                        int index3 = mesh.Triangles[num + 2];

                        Vector3[] array = new Vector3[3]
                        {
                        mesh.LocalToWorldMatrix.MultiplyPoint3x4(mesh.Vertices[index]),
                        mesh.LocalToWorldMatrix.MultiplyPoint3x4(mesh.Vertices[index2]),
                        mesh.LocalToWorldMatrix.MultiplyPoint3x4(mesh.Vertices[index3])
                        };

                        tris.Add(new STriangle3(array[0].ToSharpNav(), array[1].ToSharpNav(), array[2].ToSharpNav()));
                    }
                }
                catch (Exception ex)
                {
                    Chat.WriteLine($"Exception occured: {ex.Message} (triangle issue), continuing export");
                    continue;
                }
            }

            return tris;
        }
    }
}
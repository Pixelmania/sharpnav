﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using System.Collections.Generic;

namespace AOSharp.Pathfinding
{
    public class PolyCircle
    {
        public List<Vector3> Verts;
        public Vector3 Center;
        public float Radius;

        public PolyCircle(Vector3 center, float radius, float numSegments = 16)
        {
            Quaternion rotQuaternion = Quaternion.AngleAxis(360.0f / numSegments, Vector3.Up);
            Vector3 vertexStart = new Vector3(radius, 0, 0);

            List<Vector3> segments = new List<Vector3>();

            for (int i = 0; i < numSegments; i++)
            {
                Vector3 rotatedPoint = rotQuaternion * vertexStart;

                segments.Add(center + vertexStart);
                segments.Add(center + rotatedPoint);
                vertexStart = rotatedPoint;
            }

            Verts = segments;
            Center = center;
            Radius = radius;
        }

        public bool Intersect(PolyCircle polyCircle)
        {
            var center = polyCircle.Center;
            center.Y = 0;
            float tolerance = 0.001f;
            float radiusSquared = polyCircle.Radius * polyCircle.Radius;

            foreach (var vertex in Verts)
            {
                var vert = vertex;
                vert.Y = 0;

                float distanceSquared = (vert - center).SqrMagnitude;
                if (distanceSquared <= radiusSquared - tolerance)
                    return true;
            }

            return false;
        }

        public void Draw(Vector3 color)
        {
            for (int i = 0; i < Verts.Count - 1; i++)
            {
                Debug.DrawLine(Verts[i], Verts[i + 1], color);
                Debug.DrawSphere(Verts[i], 0.1f, color);
            }

            Debug.DrawSphere(Verts[Verts.Count - 1], 0.1f, color);
        }
    }

}

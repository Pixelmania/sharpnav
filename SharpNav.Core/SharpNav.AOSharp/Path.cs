﻿using AOSharp.Core;
using AOSharp.Common.GameData;
using System.Collections.Generic;
using System;
using System.Linq;
using SharpNav.Pathfinding;
using AOSharp.Core.UI;

namespace AOSharp.Pathfinding
{

    public class Path
    {
        private float _nodeReachedDist;
        private float _destinationReachedDist;
        public bool StopAtDest = true;
        public bool Initialized = false;
        public Queue<StraightPathVertex> Waypoints = new Queue<StraightPathVertex>();
        public Action DestinationReachedCallback;

        public Path(StraightPathVertex destination, float nodeReachedDist, float destinationReachedDist) : this(new List<StraightPathVertex>() { destination }, nodeReachedDist, destinationReachedDist)
        {
        }

        public Path(List<StraightPathVertex> waypoints, float nodeReachedDist, float destinationReachedDist)
        {
            _nodeReachedDist = nodeReachedDist;
            _destinationReachedDist = destinationReachedDist;

            SetWaypoints(waypoints);
        }

        public virtual void OnPathStart()
        {
            Initialized = true;
        }

        public virtual void OnPathFinished()
        {
            DestinationReachedCallback?.Invoke();
        }


        protected void SetWaypoints(List<StraightPathVertex> waypoints)
        {
            Waypoints.Clear();

            int count = 0;

            for (int i = 0; i < waypoints.Count; i++)
            {
                var currWaypoint = waypoints[i];

                if (currWaypoint.Flags == StraightPathFlags.OffMeshConnection && DynelManager.LocalPlayer.Position.DistanceFrom(currWaypoint.Position) <= _nodeReachedDist && count == i)
                {
                    count++;
                    continue;
                }

                Waypoints.Enqueue(currWaypoint);
            }
        }

        internal bool PeekLastWaypoint(out StraightPathVertex waypoint)
        {
            if (Waypoints.Count == 0)
            {
                waypoint = default;
                return false;
            }

            waypoint = Waypoints.LastOrDefault();
            return true;
        }

        internal PathingPhase GetNextWaypoint(out StraightPathVertex waypoint)
        {
            waypoint = default;

            if (Waypoints.Count == 0)
            {
                return PathingPhase.EndReached;
            }

            Vector3 playerPos = DynelManager.LocalPlayer.Position;
            playerPos.Y = 0f;

            while (Waypoints.Count > 0)
            {
                Vector3 targetPos = Waypoints.Peek().Position;
                targetPos.Y = 0f;

                if (playerPos.DistanceFrom(targetPos) >= (Waypoints.Count > 1 ? _nodeReachedDist : _destinationReachedDist))
                {
                    waypoint = Waypoints.Peek();
                    return PathingPhase.Traversing;
                }
                else
                {
                    waypoint = Waypoints.Dequeue();
                    return PathingPhase.NewPointReached;
                }
            }

            return PathingPhase.EndReached;
        }

        internal float Length()
        {
            Vector3 lastWaypoint = DynelManager.LocalPlayer.Position;
            float pathLength = 0.0f;

            foreach (StraightPathVertex waypoint in Waypoints)
            {
                pathLength += Vector3.Distance(lastWaypoint, waypoint.Position);
                lastWaypoint = waypoint.Position;
            }

            return pathLength;
        }

        internal void Draw()
        {
            Vector3 lastWaypoint = DynelManager.LocalPlayer.Position;

            foreach (StraightPathVertex waypoint in Waypoints)
            {
                Debug.DrawLine(lastWaypoint, waypoint.Position, DebuggingColor.Yellow);
                lastWaypoint = waypoint.Position;
            }
        }
    }
}

public enum PathingPhase
{
    Traversing,
    NewPointReached,
    EndReached
}
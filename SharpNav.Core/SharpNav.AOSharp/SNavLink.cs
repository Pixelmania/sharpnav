﻿using AOSharp.Common.GameData;

namespace AOSharp.Pathfinding
{
    public class SNavLink
    {
        public Vector3 Position;
        public string Name;
        public int DestId;
        public LinkType LinkType;

        public SNavLink(Vector3 position, string name, LinkType linkType, int destId)
        {
            Position = position;
            Name = name;
            DestId = destId;
            LinkType = linkType;
        }
    }

    public enum LinkType
    {
        Portal,
        Elevator
    }
}
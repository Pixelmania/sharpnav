﻿using AOSharp.Common.GameData;

namespace AOSharp.Pathfinding
{
    public class SMovementControllerSettings
    {
        public SNavMeshSettings NavMeshSettings;
        public SPathSettings PathSettings;

        public SMovementControllerSettings(SNavMeshSettings navMeshSettings, SPathSettings pathSettings)
        {
            NavMeshSettings = navMeshSettings;
            PathSettings = pathSettings;    
        }

        public SMovementControllerSettings(SNavMeshSettings navMeshSettings)
        {
            NavMeshSettings = navMeshSettings;
            PathSettings = SPathSettings.Default;
        }

        public SMovementControllerSettings(SPathSettings pathSettings)
        {
            NavMeshSettings = SNavMeshSettings.Default;
            PathSettings = pathSettings;
        }

        public SMovementControllerSettings()
        {
            NavMeshSettings = SNavMeshSettings.Default;
            PathSettings = SPathSettings.Default;
        }
    }

    public class SNavMeshSettings
    {
        /// <summary>
        /// Should draw NavMesh
        /// </summary>
        public bool DrawNavMesh;

        /// <summary>
        /// NavMesh draw distance (meters)
        /// </summary>
        public int DrawDistance;

        public static SNavMeshSettings Default
        {
            get
            {
                var settings = new SNavMeshSettings();

                settings.DrawNavMesh = true;
                settings.DrawDistance = 20;

                return settings;
            }
        }
    }

    public class SPathSettings
    {
        /// <summary>
        /// Should draw given path
        /// </summary>
        public bool DrawPath;

        /// <summary>
        /// Minimum angular velocity (turn speed) for a character velocity of 0
        /// </summary>
        public float MinRotSpeed;

        /// <summary>
        /// Maximum angular velocity (turn speed) for a character velocity of 13
        /// </summary>
        public float MaxRotSpeed;

        /// <summary>
        /// Tick rate for rotational update (ms)
        /// </summary>
        public int RotUpdate;

        /// <summary>
        /// Movement Action update (ms)
        /// </summary>
        public int MovementUpdate;

        /// <summary>
        /// Tick rate for unstuck update (ms)
        /// </summary>
        public int UnstuckUpdate;

        /// <summary>
        /// Unstuck threshold (meters)
        /// </summary>
        public float UnstuckThreshold;

        /// <summary>
        /// Path search radius (meters)
        /// </summary>
        public float PathRadius;

        /// <summary>
        /// Path search radius (meters)
        /// </summary>
        public Vector3 Extents;

        public static SPathSettings Default
        {
            get
            {
                var settings = new SPathSettings();

                settings.DrawPath = true;
                settings.MinRotSpeed = 10;
                settings.MaxRotSpeed = 50;
                settings.UnstuckUpdate = 5000;
                settings.UnstuckThreshold = 2f;
                settings.RotUpdate = 10;
                settings.MovementUpdate = 200;
                settings.PathRadius = 0.3f;
                settings.Extents = new Vector3(3f, 3f, 3f);
                return settings;
            }
        }
    }
}

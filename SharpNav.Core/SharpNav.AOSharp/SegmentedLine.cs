﻿using AOSharp.Common.GameData;
using AOSharp.Core;
using AOSharp.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AOSharp.Pathfinding
{
    public class SegmentedLine
    {
        public List<HullCutRef> Verts;

        public Vector3 Start => Verts.FirstOrDefault().Cut;
        public Vector3 End => Verts.LastOrDefault().Cut;

        public void RemoveLastVert() => Verts.RemoveAt(Verts.Count - 1);
        public void RemoveFirstVert() => Verts.RemoveAt(0);

        public SegmentedLine()
        {
            Verts = new List<HullCutRef>();
        }

        public SegmentedLine(Vector3 startPos, Vector3 endPos)
        {
            Verts = new List<HullCutRef> { new HullCutRef(null, startPos), new HullCutRef(null, endPos) };
        }

        public void MergeByDistance()
        {
            Verts = Verts.Distinct().ToList();
        }

        public void Append(SegmentedLine line)
        {
            Verts.AddRange(line.Verts);
        }

        public void Split(Vector3 splitPoint, ConvexHull hullRef)
        {
            if (splitPoint == Start || splitPoint == End || Verts.Any(x=>x.Cut == splitPoint))
                return;

            Verts.Add(new HullCutRef(hullRef, splitPoint));
            Verts = Verts.OrderBy(x => Vector3.Distance(x.Cut, Start)).ToList();
        }

        public void Draw(Vector3 color)
        {
            for (int i = 0; i < Verts.Count - 1; i++)
            {

                Debug.DrawLine(Verts[i].Cut + Vector3.Up, Verts[i + 1].Cut + Vector3.Up, color);
                Debug.DrawSphere(Verts[i].Cut + Vector3.Up, 0.15f, color);
            }

            Debug.DrawSphere(Start + Vector3.Up, 0.35f, DebuggingColor.Green);
            Debug.DrawSphere(End + Vector3.Up, 0.3f, DebuggingColor.LightBlue);
        }
    }
}
